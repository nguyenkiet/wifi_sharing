package com.kietnt.freewifi.layout;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.layout.adapter.PlaceSingleAdapter;
import com.kietnt.freewifi.model.Place;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.analytics.GoogleAnalytics;

public class PlaceDetailsActivity extends ListActivity
{

    private PlaceSingleAdapter adapter;
    private InterstitialAd interstitial;
    private ListView listView;
    private AdView mAdView;
    private Place place;
    private boolean isActiveWifi; 

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.place_single);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        place = (Place)getIntent().getParcelableExtra("place");
        String active = getIntent().getStringExtra("active");
        isActiveWifi = false;
        if(active != null && active.equals("active"))
        {
        	isActiveWifi = true;
        }  
        listView = getListView(); 
        adapter = new PlaceSingleAdapter(this, place, isActiveWifi);
        listView.setAdapter(adapter);
        SessionManager.getInstance().setIncreaseTotalViewPlace();
        if (SessionManager.getInstance().getTotalViewPlace() % 5 == 0)
        {
            interstitial = new InterstitialAd(this);
            interstitial.setAdUnitId(SessionManager.getInterstitialAdID());
            AdRequest adrequest1 = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
            interstitial.loadAd(adrequest1);
            interstitial.setAdListener(new AdListener() 
            {

                public void onAdLoaded()
                {
                    super.onAdLoaded();
                    interstitial.show();
                }
            });
        }
        mAdView = new AdView(this);
        mAdView.setAdUnitId(SessionManager.getAdBannerID());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        ((RelativeLayout)findViewById(R.id.adView)).addView(mAdView);
        AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
        mAdView.loadAd(adrequest);
    }

    public void onDestroy()
    {
        if (mAdView != null)
        {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 16908332: 
            finish();
            break;
        }
        return true;
    }

    public void onPause()
    {
        if (mAdView != null)
        {
            mAdView.pause();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (mAdView != null)
        {
            mAdView.resume();
        }
    }

    protected void onStart()
    {
        super.onStart();
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    protected void onStop()
    {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }

}