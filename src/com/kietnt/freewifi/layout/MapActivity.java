package com.kietnt.freewifi.layout;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.MenuItem;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.model.Place;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class MapActivity extends FragmentActivity
{
    private class GetPlacesTask extends AsyncTask
    {

        private Context mContext;

        protected Object doInBackground(Object ... aobj)
        {
            return doInBackground((Void[])aobj);
        }

        protected List doInBackground(Void avoid[])
        {
            Location location = new Location("");
            location.setLatitude(lastMapCenter.latitude);
            location.setLongitude(lastMapCenter.longitude);
            return DatabaseHelper.getInstance(mContext).getPlacesAround(location, 0.20000000000000001D);
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            updateMapWithPlaces(list);
        } 

        public GetPlacesTask(Context context)
        {
            mContext = context;
        }
    }


    private LatLng lastMapCenter;
    private LocationClient locationClient;
    private GoogleMap map;
    private List<Marker> markers;
    private List<Place> placeList;

    public MapActivity()
    {
    }

    private void updateMapWithPlaces(List list)
    {
        map.clear();
        markers.clear();
        placeList.clear();
        Place place;
        for (Iterator iterator = list.iterator(); iterator.hasNext(); placeList.add(place))
        {
            place = (Place)iterator.next();
            Marker marker = map.addMarker((new MarkerOptions()).position(place.getLatLng()).title(place.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)).snippet(getString(R.string.infowindow_viewmore)));
            markers.add(marker);
        }

        Log.d("mapview", "map pin updated");
    }
 
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle); 
        setContentView(R.layout.map_full);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        setupMapIfNeeded();
        markers = new ArrayList<Marker>();
        placeList = new ArrayList<Place>();
        locationClient = new LocationClient(this, onSucessed, onFailure);
        if (bundle != null)
        {
            double d = bundle.getDouble("lastLat", 0.0D);
            double d1 = bundle.getDouble("lastLng", 0.0D);
            if (d != 0.0D)
            {
                lastMapCenter = new LatLng(d, d1);
            }
        }
    }


    OnConnectionFailedListener onFailure =  new com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener() {		
		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			 
		}
	};
	ConnectionCallbacks onSucessed = new ConnectionCallbacks() {
		
		public void onDisconnected() {
		 
		}
		
		public void onConnected(Bundle arg0) 
		{ 
			if (lastMapCenter != null)
	        {
	            setupMapIfNeeded();
	            if (map != null)
	            {
	                map.moveCamera(CameraUpdateFactory.newLatLng(lastMapCenter));
	            }
	        } else
	        {
	            Location location = SessionManager.getInstance().getLastLocation();
	            if (location != null)
	            {
	                setupMapIfNeeded();
	                if (map != null)
	                {
	                    map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
	                    return;
	                }
	            }
	        }
		}
	};

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 16908332: 
            finish();
            break;
        }
        return true;
    }

    protected void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        if (lastMapCenter != null)
        {
            bundle.putDouble("lastLat", lastMapCenter.latitude);
            bundle.putDouble("lastLng", lastMapCenter.longitude);
        }
    }

    public void onStart()
    {
        super.onStart();
        if (locationClient != null)
        {
            locationClient.connect();
        }
    }

    public void onStop()
    {
        if (locationClient != null)
        {
            locationClient.disconnect();
        }
        super.onStop();
    }

    public void setupMapIfNeeded()
    {
        if (map == null)
        {
            map = ((MapFragment)getFragmentManager().findFragmentById(R.id.mapFragment)).getMap();
            if (map != null)
            {
                map.setMyLocationEnabled(true);
                map.setOnCameraChangeListener(new com.google.android.gms.maps.GoogleMap.OnCameraChangeListener() {

                    public void onCameraChange(CameraPosition cameraposition)
                    {
                        Log.d("mapview", "camera changed");
                        if (lastMapCenter == null)
                        {
                            lastMapCenter = cameraposition.target;
                            (new GetPlacesTask(getApplicationContext())).execute(new Void[0]);
                        } 
                        else
                        {
                            Location location = new Location("");
                            location.setLatitude(lastMapCenter.latitude);
                            location.setLongitude(lastMapCenter.longitude);
                            Location location1 = new Location("");
                            location1.setLatitude(cameraposition.target.latitude);
                            location1.setLongitude(cameraposition.target.longitude);
                            float f = location.distanceTo(location1);
                            Log.d("mapview", (new StringBuilder()).append("moved ").append(f).append(" meters").toString());
                            if (f > 200F)
                            {
                                lastMapCenter = cameraposition.target;
                                (new GetPlacesTask(getApplicationContext())).execute(new Void[0]);
                                return;
                            }
                        }
                    }
                });
                map.setOnInfoWindowClickListener(new com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener() 
                {
 
                    public void onInfoWindowClick(Marker marker)
                    {
                        Log.d("mapview", (new StringBuilder()).append("clicked infowindow of ").append(marker.getTitle()).toString());
                        Intent intent = new Intent(getApplicationContext(), PlaceDetailsActivity.class);
                        @SuppressWarnings("rawtypes")
						Iterator iterator = placeList.iterator();
                        do
                        {
                            if (!iterator.hasNext())
                            {
                                break;
                            }
                            Place place = (Place)iterator.next();
                            if (place.getLatitude() != marker.getPosition().latitude || place.getLongitude() != marker.getPosition().longitude)
                            {
                                continue;
                            }
                            intent.putExtra("place", place);
                            startActivity(intent);
                            break;
                        } while (true);
                    }
                });
            }
        }
    }
}