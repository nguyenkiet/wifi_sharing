package com.kietnt.freewifi.layout;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.DatabaseManager;
import com.kietnt.freewifi.helper.LevenshteinDistance;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.layout.adapter.PlaceListAdapter;
import com.kietnt.freewifi.model.Place;
import com.kietnt.freewifi.model.Vote;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class HomeFragment extends Fragment
{
	public class VisibleWifiScanner extends AsyncTask
    {
		private Context scanerContext = null;
		private ListView scanerListView = null;
		
        protected Object doInBackground(Object aobj[])
        {
            // Load wifi list here
        	ConnectionManager connectionmanager = ConnectionManager.getInstance(scanerContext);
        	connectionmanager.scanWiFi(new com.kietnt.freewifi.helper.ConnectionManager.GetWiFiResult() 
            { 
                public void done(List listWifi)
                {
                	try
                	{
	                	if(listWifi == null) return;
	                	if(adapter != null) adapter.setListWifi(listWifi);
	                	int counter = scanerListView.getAdapter().getCount();
	                	for (int j = 0; j < counter; j++)
	                    { 
	        				View v = scanerListView.getChildAt(j);
	        				if(v == null) continue;
	        				v.setBackground(getResources().getDrawable(R.drawable.bacgroundwhiterounded));
	        				TextView acText = (TextView) v.findViewById(R.id.itemIsActive);
	        				TextView placeName = (TextView) v.findViewById(R.id.placeName);
	        				ImageView signalLevel = (ImageView) v.findViewById(R.id.wifiSignalImgView);
	        				if(acText == null) continue;
	        				if(placeName == null) continue;
	        				if(signalLevel == null) continue;
	        				String itemName = placeName.getText().toString();
	        				acText.setText("");
	        				signalLevel.setVisibility(View.GONE);
		                	for (int i = 0; i < listWifi.size(); i++)
		                    {
		                		ScanResult res = ((ScanResult)listWifi.get(i));	                		
	                			if(itemName.equals(res.SSID))
	                			{ 
	                				int strensgth = ConnectionManager.calculateWifiSingalStrength(res.level * -1);
	                				signalLevel.setVisibility(View.VISIBLE);
	                				signalLevel.setBackgroundResource(PlaceListAdapter.getSinalLevelResourceId(strensgth));
	                           	 	v.setBackground(getResources().getDrawable(R.drawable.bacgroundwhiterounded_active));	                           	
	                           	 	acText.setText("active");                      
	                			}
		                    }
	                    }
                	}
                	catch (Exception ex)
                	{
                		// Do nothings
                	}
                } 
            });
            return null;
        }

        @SuppressWarnings("unchecked")
		protected void onPostExecute(Object obj)
        {
            super.onPostExecute(obj);
        }

        protected void onPreExecute()
        {
            super.onPreExecute();
        }

        public VisibleWifiScanner(Context context, ListView list)
        {
        	scanerListView  = list;
        	scanerContext = context;
            Log.d("test", "in asynck task");
        }
    }
    @SuppressWarnings("rawtypes")
	private class DummyBackgroundTask extends AsyncTask
    {

        protected Object doInBackground(Object ... aobj)
        {
            return doInBackground((Void[])aobj);
        }

        protected List doInBackground(Void avoid[])
        {
            Log.d("parse", (new StringBuilder()).append("location ").append(SessionManager.getInstance().getLastLocation().getLatitude()).append(",").append(SessionManager.getInstance().getLastLocation().getLongitude()).toString());
            return DatabaseHelper.getInstance(getActivity()).getPlacesAround(SessionManager.getInstance().getLastLocation(), 0.20000000000000001D);
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        @SuppressWarnings("unchecked")
		protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            onRefreshComplete(list);
        }
    }


    private static final String LOG_TAG = HomeFragment.class.getSimpleName();
    private PlaceListAdapter adapter;
    private boolean isWiFiNotifyShowed;
    private ListView listView;
    private LocationClient locationClient;
    private AdView mAdView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @SuppressWarnings("rawtypes")
	private ArrayList placeList;
    private View wifiNotification; 

    private void createNewPlace(final ProgressDialog progressDialog, String s, String s1)
    {
        Log.d("parse", "new place");
        Place place = new Place();
        place.createNewWithLocation(getActivity(), locationClient.getLastLocation(), s, s1, new com.kietnt.freewifi.model.Place.OnCreateNewPlaceCallback() 
        {
            public void done(Place place, Vote vote)
            {
                if (place != null)
                {
                    DatabaseHelper.getInstance(getActivity()).addPlace(place);
                    DatabaseHelper.getInstance(getActivity()).addVote(vote);
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Send_WiFi_succeed), 0).show();
                        }
                    });
                    return;
                } 
                else
                {
                    getActivity().runOnUiThread(new Runnable() {

                        public void run()
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Send_WiFi_failed_please_try_again), 0).show();
                        }
                    });
                    return;
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
	private void initiateRefresh()
    {
        Log.i(LOG_TAG, "initiateRefresh");
        if (SessionManager.getInstance().isFirstTimeUsingApp() || !isLocationServicesEnabled())
        {
            return;
        }
        if (locationClient.getLastLocation() != null)
        {
            SessionManager.getInstance().setLastLocation(locationClient.getLastLocation());
            (new DummyBackgroundTask()).execute(new Void[0]);
            return;
        }
        try
        {
            Toast.makeText(getActivity(), R.string.GPS_not_received, 0).show();
            mSwipeRefreshLayout.setRefreshing(false);
            return;
        }
        catch (IllegalStateException illegalstateexception)
        {
            Toast.makeText(getActivity(), R.string.GPS_not_received, 0).show();
        }
        mSwipeRefreshLayout.setRefreshing(false);
        return; 
    }

    private boolean isLocationServicesEnabled()
    {
        if (ConnectionManager.getInstance(getActivity()).locationServiceEnabled())
        {
            if (locationClient == null)
            {
                return false;
            } else
            {
                Log.d("location", "location is on");
                return true;
            }
        } else
        {
            Log.d("location", "location is off");
            (new android.app.AlertDialog.Builder(getActivity())).setTitle(R.string.GPS_is_OFF).setMessage(R.string.Please_turn_on_GPS).setNegativeButton(R.string.i_will_active_GPS, new android.content.DialogInterface.OnClickListener() 
            {
                public void onClick(DialogInterface dialoginterface, int i)
                {
                }
            }).create().show();
            return false;
        }
    }

    private void onRefreshComplete(List list)
    {
        Log.i(LOG_TAG, "onRefreshComplete");
        Log.d("parse", (new StringBuilder()).append("nearby ").append(list.size()).append(" places").toString());
        adapter.setPlaceList(list);
        adapter.notifyDataSetChanged();
        placeList = new ArrayList(list);
        mSwipeRefreshLayout.setRefreshing(false);
        if (!isWiFiNotifyShowed)
        {
            final String connectedWiFi = ConnectionManager.getInstance(getActivity()).getConnectedWiFi();
            if (connectedWiFi != null && SessionManager.getInstance().canShareNow())
            {
                isWiFiNotifyShowed = true;
                (new Handler()).postDelayed(new Runnable() {
                    public void run()
                    {
                        showWiFiNotification(connectedWiFi.replace("\"", ""));
                    }
                }, 2000L);
            }
        }
    }

    private void sendNewPassword(final ProgressDialog progressDialog, String s, String s1, Place place, boolean voting)
    {
        Log.d("parse", "send new pass"); 
        place.sendNewPassword(getActivity(), s, s1, place, voting, false, new com.kietnt.freewifi.model.Place.OnSendNewPasswordCallback() 
        {
            public void done(boolean flag, Vote vote)
            {
                if (flag)
                {
                    if (vote != null)
                    {
                        DatabaseHelper.getInstance(getActivity()).addVote(vote);
                    }
                    getActivity().runOnUiThread(new Runnable() {


                        public void run()
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Send_WiFi_succeed), 0).show();
                        }
                    });
                    return;
                } 
                else
                {
                    getActivity().runOnUiThread(new Runnable() {
                        public void run()
                        {
                            progressDialog.dismiss();
                            Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Send_WiFi_failed_please_try_again), 0).show();
                        }
                    });
                    return;
                }
            }
        });
    }

    private void shareWiFi(final String connectedWiFi, final String password, final boolean voting)
    {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage(getString(R.string.Sending));
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(0);
        progressDialog.setCancelable(false);
        progressDialog.show();
        DatabaseManager.getInstance().fetchPlaceNearbyForSharing(locationClient.getLastLocation(), 0.050000000000000003D, new com.kietnt.freewifi.helper.DatabaseManager.FindParseDataCallback() 
        {
            public void done(boolean flag, List list)
            {
                if (flag)
                {
                    if (list == null || list.size() == 0)
                    {
                        createNewPlace(progressDialog, connectedWiFi, password);
                        return;
                    }
                    final ArrayList arraylist = new ArrayList();
                    Iterator iterator = list.iterator();
                    do
                    {
                        if (!iterator.hasNext())
                        {
                            break;
                        }
                        Place place = (Place)iterator.next();
                        if (LevenshteinDistance.similarity(connectedWiFi.toUpperCase(), place.getName().toUpperCase()) >= 0.40000000000000002D)
                        {
                            if (place.getName().trim().equalsIgnoreCase(connectedWiFi))
                            {
                                sendNewPassword(progressDialog, place.getParseID(), password, place, voting);
                                return;
                            }
                            arraylist.add(place);
                        }
                    } while (true);
                    if (arraylist.size() == 0)
                    {
                        createNewPlace(progressDialog, connectedWiFi, password);
                        return;
                    }
                    CharSequence acharsequence[] = new CharSequence[arraylist.size()];
                    for (int i = 0; i < arraylist.size(); i++)
                    {
                        acharsequence[i] = ((Place)arraylist.get(i)).getName();
                    }

                    android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.you_are_at_these_place).setItems(acharsequence, new android.content.DialogInterface.OnClickListener() 
                    { 
                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            dialoginterface.dismiss();
                            progressDialog.show();
                            Place itemPlace = (Place)arraylist.get(i);
                            sendNewPassword(progressDialog, itemPlace.getParseID(), password, itemPlace, voting);
                        } 
                    }).setPositiveButton(R.string.no_create_new, new android.content.DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            dialoginterface.dismiss();
                            progressDialog.show();
                            createNewPlace(progressDialog, connectedWiFi, password);
                        }
                    }).setNegativeButton(R.string.Dont_Share, new android.content.DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            progressDialog.dismiss();
                            dialoginterface.dismiss();
                        }
                    });
                    progressDialog.hide();
                    builder.create().show();
                    return;
                } else
                {
                    progressDialog.dismiss();
                    return;
                }
            }
        });
    } 
    
    OnConnectionFailedListener onFailure =  new com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener() {		
		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			Log.d(LOG_TAG, "location failed");
		}
	};
	ConnectionCallbacks onSucessed = new ConnectionCallbacks() {
		
		public void onDisconnected() {
			Log.d(LOG_TAG, "location disconnected");
		}
		 
		public void onConnected(Bundle arg0) {
			Log.d(LOG_TAG, "location connected");
	        if (placeList != null && placeList.size() == 0)
	        {
	            initiateRefresh();
	        }
		}
	};
	
    @SuppressWarnings("unchecked")
	public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        View view = layoutinflater.inflate(R.layout.fragment_home, viewgroup, false);
        locationClient = new LocationClient(getActivity(), onSucessed, onFailure);
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.placeListSwipeRefresh);
        //mSwipeRefreshLayout.setColorSchemeResources(R.color.swipe_color_1, R.color.swipe_color_2, R.color.swipe_color_3, R.color.swipe_color_4);
        listView = (ListView)view.findViewById(R.id.placeListView);
        if (bundle != null)
        {        	
            placeList = bundle.getParcelableArrayList("places");
        }
        if (placeList == null)
        {
            placeList = new ArrayList();
        }
        adapter = new PlaceListAdapter(getActivity(), placeList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView adapterview, View view1, int i, long l)
            {
                Intent intent = new Intent(getActivity(), PlaceDetailsActivity.class);
                TextView tv = (TextView) view1.findViewById(R.id.itemIsActive);
                intent.putExtra("place", (Place)adapter.getItem(i));
                if(tv != null)
                {
                	intent.putExtra("active", tv.getText());
                }
                else
                {
                	intent.putExtra("active", "");
                }
                startActivity(intent);
            }
        });
        listView.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() 
        {
            public boolean onItemLongClick(AdapterView adapterview, View view1, int i, long l)
            {
                return true;
            }
        });
        isWiFiNotifyShowed = false;
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(SessionManager.getAdBannerID());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        ((RelativeLayout)view.findViewById(R.id.adView)).addView(mAdView);
        AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
        mAdView.loadAd(adrequest);
        // Start scanner wifi around here
        (new VisibleWifiScanner(view.getContext(), listView)).execute();
        return view;
    }

    public void onDestroy()
    {
        if (mAdView != null)
        {
            mAdView.destroy();
        }
        super.onDestroy();
    }
 

    public void onPause()
    {
        if (mAdView != null)
        {
            mAdView.pause();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (mAdView != null)
        {
            mAdView.resume();
        }
    }

    public void onSaveInstanceState(Bundle bundle)
    {
        super.onSaveInstanceState(bundle);
        bundle.putParcelableArrayList("places", placeList);
    }

    public void onStart()
    {
        super.onStart();
        if (locationClient != null)
        {
            locationClient.connect();
        }
    }

    public void onStop()
    {
        if (locationClient != null)
        {
            locationClient.disconnect();
        }
        if (wifiNotification != null)
        {
            try
            {
                ((ViewGroup)getView()).removeView(wifiNotification);
                wifiNotification = null;
            }
            catch (NullPointerException nullpointerexception)
            {
                Log.e(LOG_TAG, nullpointerexception.getMessage());
            }
        }
        super.onStop();
    }

    public void onViewCreated(View view, Bundle bundle)
    {
        super.onViewCreated(view, bundle);
        mSwipeRefreshLayout.setOnRefreshListener(new android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener() 
        {
            public void onRefresh()
            {
                Log.i(HomeFragment.LOG_TAG, "onRefresh called from SwipeRefreshLayout");
                if (!SessionManager.getInstance().isFirstTimeUsingApp())
                {
                    initiateRefresh();
                }
            }
        });
    }

    public void showWiFiNotification(final String connectedWiFi)
    {
        if (!locationClient.isConnected())
        {
            return;
        } else
        {
            Log.d("view", "add view");
            wifiNotification = ((LayoutInflater)getActivity().getApplicationContext().getSystemService("layout_inflater")).inflate(R.layout.wifi_notification, null);
            android.widget.RelativeLayout.LayoutParams layoutparams = new android.widget.RelativeLayout.LayoutParams(-1, (int)TypedValue.applyDimension(1, 55F, getResources().getDisplayMetrics()));
            layoutparams.addRule(12);
            wifiNotification.setLayoutParams(layoutparams);
            TextView textview = (TextView)wifiNotification.findViewById(R.id.txtWiFiName);
            ImageButton imagebutton = (ImageButton)wifiNotification.findViewById(R.id.btnCloseWifiNotify);
            Button button = (Button)wifiNotification.findViewById(R.id.btnShareWiFi);
            textview.setText(connectedWiFi);
            final ViewGroup viewGroup = (ViewGroup)getView();
            viewGroup.addView(wifiNotification);
            View view = wifiNotification;
            float af[] = new float[2];
            af[0] = getView().getHeight();
            af[1] = getView().getHeight() - (int)TypedValue.applyDimension(1, 55F, getResources().getDisplayMetrics());
            ObjectAnimator objectanimator = ObjectAnimator.ofFloat(view, "y", af);
            objectanimator.setDuration(500L);
            objectanimator.start();
            imagebutton.setOnClickListener(new android.view.View.OnClickListener() 
            { 
                public void onClick(View view1)
                {
                    View view2 = wifiNotification;
                    float af1[] = new float[2];
                    af1[0] = getView().getHeight() - (int)TypedValue.applyDimension(1, 55F, getResources().getDisplayMetrics());
                    af1[1] = getView().getHeight();
                    ObjectAnimator objectanimator1 = ObjectAnimator.ofFloat(view2, "y", af1);
                    objectanimator1.setDuration(300L);
                    objectanimator1.start();
                    objectanimator1.addListener(new android.animation.Animator.AnimatorListener() {
 
                        public void onAnimationCancel(Animator animator)
                        {
                        }

                        public void onAnimationEnd(Animator animator)
                        {
                            viewGroup.removeView(wifiNotification);
                            wifiNotification = null;
                        }

                        public void onAnimationRepeat(Animator animator)
                        {
                        }

                        public void onAnimationStart(Animator animator)
                        {
                        } 
                    });
                }
            });
            button.setOnClickListener(new android.view.View.OnClickListener() 
            {
                public void onClick(View view1)
                {
                    viewGroup.removeView(wifiNotification);
                    wifiNotification = null;
                    if (isLocationServicesEnabled())
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                        View view2 = getActivity().getLayoutInflater().inflate(R.layout.wifi_sharing, null);
                        TextView textview1 = (TextView)view2.findViewById(R.id.txtWiFiName);
                        final EditText txtPassword = (EditText)view2.findViewById(R.id.txtPassword);
                        textview1.setText(connectedWiFi);
                        builder.setView(view2).setPositiveButton(R.string.Send, new android.content.DialogInterface.OnClickListener() 
                        {                  
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                ((InputMethodManager)getActivity().getSystemService("input_method")).hideSoftInputFromWindow(txtPassword.getWindowToken(), 0);
                                dialoginterface.dismiss();
                                if (txtPassword.getText().toString().equals(""))
                                {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.Please_enter_WiFi_password), 0).show();
                                    return;
                                } else
                                {
                                    shareWiFi(connectedWiFi, txtPassword.getText().toString(), false);
                                    return;
                                }
                            } 
                        }).setNegativeButton(R.string.Dont_Share, new android.content.DialogInterface.OnClickListener() {
  

                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                ((InputMethodManager)getActivity().getSystemService("input_method")).hideSoftInputFromWindow(txtPassword.getWindowToken(), 0);
                                dialoginterface.dismiss();
                            } 
                        });
                        builder.create().show();
                        ((InputMethodManager)getActivity().getSystemService("input_method")).toggleSoftInput(2, 0);
                    }
                }
            });
            return;
        }
    } 
}