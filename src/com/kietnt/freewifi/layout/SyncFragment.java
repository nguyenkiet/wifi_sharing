package com.kietnt.freewifi.layout;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.DatabaseManager;
import com.kietnt.freewifi.helper.SessionManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import java.util.List;

public class SyncFragment extends Fragment
{
    private class SaveDataTask extends AsyncTask
    {

        private String city;
        private String country;

        protected Integer doInBackground(List alist[])
        {
            DatabaseHelper.getInstance(getActivity()).addPlaceList(alist[0]);
            Log.d("parse", "db saved");
            return Integer.valueOf(alist[0].size());
        }

        protected Object doInBackground(Object ... aobj)
        {
            return doInBackground((List[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            super.onPostExecute(integer);
            if (integer.intValue() == 1000)
            {
                Log.d("parse", "continue sync");
                syncNow(city, country);
                return;
            }
            if (progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            displayInterstitial();
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        public SaveDataTask(String s, String s1)
        {
            city = s;
            country = s1;
        }
    }


    private InterstitialAd interstitial;
    private boolean isWaitingForLocation;
    private LocationClient locationClient;
    private LocationRequest locationRequest;
    private AdView mAdView;
    private ProgressDialog progressDialog;
 
    private boolean isLocationServicesEnabled()
    {
        if (ConnectionManager.getInstance(getActivity()).locationServiceEnabled())
        {
            Log.d("location", "location is on");
            return true;
        } else
        {
            Log.d("location", "location is off");
            (new android.app.AlertDialog.Builder(getActivity())).setTitle(R.string.GPS_is_OFF).setMessage(R.string.Please_turn_on_GPS).setNegativeButton(R.string.i_will_active_GPS, new android.content.DialogInterface.OnClickListener() {


                public void onClick(DialogInterface dialoginterface, int i)
                {
                }
            }).create().show();
            return false;
        }
    }

    private void showDialog()
    {
        if (progressDialog == null)
        {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage((new StringBuilder()).append(getString(R.string.Downloading)).append("\n").append(getString(R.string.Please_dont_switch_application)).toString());
            progressDialog.setIndeterminate(false);
            progressDialog.setProgressStyle(0);
            progressDialog.setCancelable(false);
        }
        progressDialog.show();
    }

    private void showError()
    {
        getActivity().runOnUiThread(new Runnable() 
        {
            public void run()
            {
                if (progressDialog != null && progressDialog.isShowing())
                {
                    progressDialog.dismiss();
                }
                Toast.makeText(getActivity(), (new StringBuilder()).append(getString(R.string.Error)).append(" ").append(getString(R.string.Please_try_again)).toString(), 0).show();
            }
        });
    }

    private void syncNow(final String city, final String country)
    {
        DatabaseManager.getInstance().fetchAllPlace(city, country, new com.kietnt.freewifi.helper.DatabaseManager.FindParseDataCallback() 
        {

            @SuppressWarnings("unchecked")
			public void done(boolean flag, List list)
            {
                if (flag)
                {
                    Log.d("parse", "sync completed!");
                    if (list == null)
                    {
                        if (progressDialog.isShowing())
                        {
                            progressDialog.dismiss();
                        }
                        displayInterstitial();
                        return;
                    }
                    if (list.size() != 0)
                    {
                        (new SaveDataTask(city, country)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new List[] {
                            list
                        });
                        return;
                    } else
                    {
                        getActivity().runOnUiThread(new Runnable() {

                            public void run()
                            {
                                if (progressDialog.isShowing())
                                {
                                    progressDialog.dismiss();
                                }
                                displayInterstitial();
                            }
                        });
                        return;
                    }
                } else
                {
                    showError();
                    return;
                }
            }
        });
    }

    public void displayInterstitial()
    {
        if (interstitial != null && interstitial.isLoaded())
        {
            interstitial.show();
        }
    }
 

    OnConnectionFailedListener onFailure =  new com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener() {		
		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			

		}
	};
	ConnectionCallbacks onSucessed = new ConnectionCallbacks() 
	{		 
		
		public void onConnected(Bundle arg0) {
			locationClient.requestLocationUpdates(locationRequest, new LocationListener() 
	        {

	            public void onLocationChanged(Location location)
	            {
	                Log.d("location", "location updated");
	                if (locationClient.getLastLocation() != null && isWaitingForLocation)
	                {
	                    isWaitingForLocation = false;
	                    SessionManager.getInstance().setLastLocation(locationClient.getLastLocation());
	                    updateDatabase();
	                }
	            }
	        });
		}

		@Override
		public void onDisconnected() {
			// TODO Auto-generated method stub
			
		}
	};
	
    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        View view = layoutinflater.inflate(R.layout.fragment_sync, viewgroup, false);
        isWaitingForLocation = false;
        locationClient = new LocationClient(getActivity(), onSucessed, onFailure);
        locationRequest = new LocationRequest();
        locationRequest.setPriority(100);
        locationRequest.setInterval(5L);
        locationRequest.setFastestInterval(1L);
        ((Button)view.findViewById(R.id.buttonSyncNow)).setOnClickListener(new android.view.View.OnClickListener() 
        {
            public void onClick(View view1)
            {
                interstitial = new InterstitialAd(getActivity());
                interstitial.setAdUnitId(SessionManager.getInterstitialAdID());
                AdRequest adrequest1 = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
                interstitial.loadAd(adrequest1);
                if (SessionManager.getInstance().haveRunningService())
                {
                    (new android.app.AlertDialog.Builder(getActivity())).setTitle(R.string.Downloading).setMessage(R.string.Service_is_running).setNegativeButton(R.string.OK, new android.content.DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            displayInterstitial();
                        }
                    }).create().show();
                } else
                if (isLocationServicesEnabled())
                {
                    showDialog();
                    isWaitingForLocation = true;
                    return;
                }
            }
        });
        ((Button)view.findViewById(R.id.buttonSyncTravel)).setOnClickListener(new android.view.View.OnClickListener() 
        {
            public void onClick(View view1)
            {
                Intent intent = new Intent(getActivity(), SyncTravelActivity.class);
                startActivity(intent);
            }
        });
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(SessionManager.getAdBannerID());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        ((RelativeLayout)view.findViewById(R.id.adView)).addView(mAdView);
        AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
        mAdView.loadAd(adrequest);
        return view;
    }

    public void onDestroy()
    {
        if (mAdView != null)
        {
            mAdView.destroy();
        }
        super.onDestroy();
    }
 
    public void onPause()
    {
        if (mAdView != null)
        {
            mAdView.pause();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (mAdView != null)
        {
            mAdView.resume();
        }
    }

    public void onStart()
    {
        super.onStart();
        locationClient.connect();
    }

    public void onStop()
    {
        locationClient.disconnect();
        super.onStop();
    }

    public void updateDatabase()
    {
        Location location = SessionManager.getInstance().getLastLocation();
        SessionManager.getLocationInfo(getActivity(), location, new com.kietnt.freewifi.helper.SessionManager.GetLocationAddressCallback() 
        {

            public void onComplete(boolean flag, String s, String s1, String s2)
            {
                if (flag)
                {
                    Log.d("geocoder", "done");
                    syncNow(s1, s2);
                    return;
                } else
                {
                    Log.d("geocoder", "fail");
                    showError();
                    return;
                }
            }
        });
    }
}