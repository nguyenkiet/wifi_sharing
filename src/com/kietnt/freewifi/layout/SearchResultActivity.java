package com.kietnt.freewifi.layout;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.layout.adapter.PlaceListAdapter;
import com.kietnt.freewifi.model.Place;
import java.util.ArrayList;
import java.util.List;

// Referenced classes of package com.kietnt.freewifi.layout:
//            PlaceDetailsActivity

public class SearchResultActivity extends Activity
{
    private class GetPlacesTask extends AsyncTask
    {

        private Context mContext;

        protected Object doInBackground(Object ...aobj)
        {
            return doInBackground((String[])aobj);
        }

        protected List doInBackground(String as[])
        {
            Log.d("parse", (new StringBuilder()).append("search ").append(as[0]).toString());
            return DatabaseHelper.getInstance(mContext).searchPlacesWithKeyword(as[0]);
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            updateResult(list);
        }

        public GetPlacesTask(Context context)
        {
            mContext = context;
        }
    }


    private PlaceListAdapter adapter;
    private ListView listView;
    private List results;

    public SearchResultActivity()
    {
    }

    private void handleIntent(Intent intent)
    {
        if ("android.intent.action.SEARCH".equals(intent.getAction()))
        {
            String s = intent.getStringExtra("query");
            (new GetPlacesTask(this)).execute(new String[] {
                s
            });
        }
    }

    private void updateResult(List list)
    {
        adapter.setPlaceList(list);
        adapter.notifyDataSetChanged();
        results = list;
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.search_place_results);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView)findViewById(R.id.search_result_list);
        if (results == null)
        {
            results = new ArrayList();
        }
        adapter = new PlaceListAdapter(this, results);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() 
        {
            public void onItemClick(AdapterView adapterview, View view, int i, long l)
            {
                Intent intent = new Intent(getApplicationContext(), PlaceDetailsActivity.class);
                intent.putExtra("place", (Place)adapter.getItem(i));
                startActivity(intent);
            }
        });
        handleIntent(getIntent());
    }

    protected void onNewIntent(Intent intent)
    {
        setIntent(intent);
        handleIntent(intent);
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 16908332: 
            finish();
            break;
        }
        return true;
    }
}