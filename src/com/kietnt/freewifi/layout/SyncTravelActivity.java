package com.kietnt.freewifi.layout;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.DatabaseManager;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.layout.adapter.SyncTravelAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class SyncTravelActivity extends Activity
{
    private class GetLocationInfoTask extends AsyncTask
    {

        private Context context;

        protected Object doInBackground(Object ... aobj)
        {
            return doInBackground((String[])aobj);
        }

        protected List doInBackground(String as[])
        {
            Geocoder geocoder = new Geocoder(context, Locale.US);
            List list;
            try
            {
                list = geocoder.getFromLocationName(as[0], 10);
                Log.d("geocoder", (new StringBuilder()).append("found result ").append(list.size()).toString());
            }
            catch (IOException ioexception)
            {
                Log.d("geocoder", (new StringBuilder()).append("error ").append(ioexception.getMessage()).toString());
                return null;
            }
            return list;
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((List)obj);
        }

        protected void onPostExecute(List list)
        {
            super.onPostExecute(list);
            updateResult(list);
        }

        public GetLocationInfoTask(Context context1)
        {
            context = context1;
        }
    }

    private class SaveDataTask extends AsyncTask
    {

        private Activity activity;
        private String city;
        private String country;

        protected Integer doInBackground(List alist[])
        {
            DatabaseHelper.getInstance(activity).addPlaceList(alist[0]);
            Log.d("parse", "db saved");
            return Integer.valueOf(alist[0].size());
        }

        protected Object doInBackground(Object ...aobj)
        {
            return doInBackground((List[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            super.onPostExecute(integer);
            if (integer.intValue() == 1000)
            {
                Log.d("parse", "continue sync");
                syncNow(city, country);
                return;
            }
            if (progressDialog.isShowing())
            {
                progressDialog.dismiss();
            }
            displayInterstitial();
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        public SaveDataTask(Activity activity1, String s, String s1)
        {
            activity = activity1;
            city = s;
            country = s1;
        }
    }


    private SyncTravelAdapter adapter;
    private InterstitialAd interstitial;
    private ListView listView;
    private ProgressDialog progressDialog;

    public SyncTravelActivity()
    {
    }

    private void showError()
    {
        (new android.app.AlertDialog.Builder(this)).setTitle(R.string.Error).setMessage(R.string.Please_try_again).setNegativeButton(R.string.OK, new android.content.DialogInterface.OnClickListener() 
        {
            public void onClick(DialogInterface dialoginterface, int i)
            {
                displayInterstitial();
            }
        }).create().show();
    }

    private void syncNow(final String city, final String country)
    {
        DatabaseManager.getInstance().fetchAllPlace(city, country, new com.kietnt.freewifi.helper.DatabaseManager.FindParseDataCallback() 
        {

            @SuppressWarnings("unchecked")
			public void done(boolean flag, List list)
            {
                if (flag)
                {
                    Log.d("parse", "sync completed!");
                    if (list == null)
                    {
                        if (progressDialog.isShowing())
                        {
                            progressDialog.dismiss();
                        }
                        displayInterstitial();
                        return;
                    }
                    if (list.size() != 0)
                    {
                        (new SaveDataTask(SyncTravelActivity.this, city, country)).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new List[] {
                            list
                        });
                        return;
                    }
                    if (progressDialog.isShowing())
                    {
                        progressDialog.dismiss();
                    }
                    displayInterstitial();
                    return;
                } else
                {
                    showError();
                    return;
                }
            }
        });
    }

    private void updateResult(List list)
    {
        if (list != null)
        {
            adapter.setAddressList(list);
            adapter.notifyDataSetChanged();
        }
    }

    public void displayInterstitial()
    {
        if (interstitial.isLoaded())
        {
            interstitial.show();
        }
    }

    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.sync_travel);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        listView = (ListView)findViewById(R.id.syncTravelListView);
        adapter = new SyncTravelAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new android.widget.AdapterView.OnItemClickListener() 
        { 
            public void onItemClick(AdapterView adapterview, View view, int i, long l)
            {
                interstitial = new InterstitialAd(SyncTravelActivity.this);
                interstitial.setAdUnitId(SessionManager.getInterstitialAdID());
                AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
                interstitial.loadAd(adrequest);
                Address address = (Address)adapter.getItem(i);
                String s;
                String s1;
                if (address.getAdminArea() == null)
                {
                    s = address.getFeatureName();
                } else
                {
                    s = address.getAdminArea();
                }
                s1 = address.getCountryName();
                Log.d("parse", (new StringBuilder()).append("sync ").append(s).append(", ").append(s1).toString());
                progressDialog = new ProgressDialog(view.getContext());
                progressDialog.setMessage((new StringBuilder()).append(view.getContext().getString(R.string.Downloading)).append("\n").append(view.getContext().getString(R.string.Please_dont_switch_application)).toString());
                progressDialog.setIndeterminate(false);
                progressDialog.setProgressStyle(0);
                progressDialog.setCancelable(false);
                progressDialog.show();
                if (s == null || s1 == null)
                {
                    showError();
                    return;
                } else
                {
                    syncNow(s, s1);
                    return;
                }
            } 
        });
        final EditText txtSearch = (EditText)findViewById(R.id.txtKeyword);
        txtSearch.addTextChangedListener(new TextWatcher() 
        {

            @SuppressWarnings("unchecked")
			public void afterTextChanged(Editable editable)
            {
                String s = txtSearch.getText().toString();
                if (s.length() >= 3)
                {
                    (new GetLocationInfoTask(SyncTravelActivity.this)).execute(new String[] {
                        s
                    });
                }
            }

            public void beforeTextChanged(CharSequence charsequence, int i, int j, int k)
            {
            }

            public void onTextChanged(CharSequence charsequence, int i, int j, int k)
            {
            }
        });
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        switch (menuitem.getItemId())
        {
        default:
            return super.onOptionsItemSelected(menuitem);

        case 16908332: 
            finish();
            break;
        }
        return true;
    }
}