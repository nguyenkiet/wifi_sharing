package com.kietnt.freewifi.layout;

import com.kietnt.freewifi.R;

import android.app.Activity;
import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class InfoFragment extends Fragment
{
 
    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        View view = layoutinflater.inflate(R.layout.fragment_info, viewgroup, false);
        TextView textview = (TextView)view.findViewById(R.id.txtVersion);
        Button button = (Button)view.findViewById(R.id.btnRateUs);
        Button button1 = (Button)view.findViewById(R.id.btnFacebook);
        Button button2 = (Button)view.findViewById(R.id.btnWebsite);
        try
        {
            textview.setText((new StringBuilder()).append(getString(R.string.Version)).append(" ").append(getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName).toString());
        }
        catch (android.content.pm.PackageManager.NameNotFoundException namenotfoundexception) { }
        button.setOnClickListener(new android.view.View.OnClickListener()
        {
            public void onClick(View view1)
            {
                String s = getActivity().getPackageName();
                try
                {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse((new StringBuilder()).append("market://details?id=").append(s).toString())));
                    return;
                }
                catch (ActivityNotFoundException activitynotfoundexception)
                {
                    startActivity(new Intent("android.intent.action.VIEW", Uri.parse((new StringBuilder()).append("http://play.google.com/store/apps/details?id=").append(s).toString())));
                }
            }
        });
        button1.setOnClickListener(new android.view.View.OnClickListener()
        {
            public void onClick(View view1)
            {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("https://www.facebook.com/wifimienphi"));
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new android.view.View.OnClickListener() 
        {
            public void onClick(View view1)
            {
                Intent intent = new Intent("android.intent.action.VIEW", Uri.parse("http://www.wifimienphi.com"));
                startActivity(intent);
            }            
        });
        return view;
    }
}