package com.kietnt.freewifi.layout;

import android.app.Fragment;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.layout.adapter.SettingAdapter;
import com.kietnt.freewifi.model.SyncInfo;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import java.util.Iterator;
import java.util.List;

public class SettingFragment extends Fragment
{
    private class FetchInfoTask extends AsyncTask
    {

        private GetTotalPlacesCallback callback;
        private Context context;

        protected Integer doInBackground(String as[])
        {
            int i;
            if (as.length > 0)
            {
                String s = as[0];
                String s1 = as[1];
                i = 0;
                if (s != null)
                {
                    i = 0;
                    if (s1 != null)
                    {
                        i = (int)DatabaseHelper.getInstance(context).getTotalPlace(s, s1);
                    }
                }
            } else
            {
                i = (int)DatabaseHelper.getInstance(context).getTotalPlace();
            }
            return Integer.valueOf(i);
        }

        protected Object doInBackground(Object ...aobj)
        {
            return doInBackground((String[])aobj);
        }

        protected void onPostExecute(Integer integer)
        {
            super.onPostExecute(integer);
            callback.done(integer.intValue());
        }

        protected void onPostExecute(Object obj)
        {
            onPostExecute((Integer)obj);
        }

        public FetchInfoTask(Context context1, GetTotalPlacesCallback gettotalplacescallback)
        {
            context = context1;
            callback = gettotalplacescallback;
        }
    }

    private static interface GetTotalPlacesCallback
    {

        public abstract void done(int i);
    }


    private SettingAdapter adapter;
    private ListView listView;
    private AdView mAdView;
 
    public View onCreateView(LayoutInflater layoutinflater, ViewGroup viewgroup, Bundle bundle)
    {
        View view = layoutinflater.inflate(R.layout.fragment_setting, viewgroup, false);
        listView = (ListView)view.findViewById(R.id.settingListView);
        adapter = new SettingAdapter(getActivity());
        listView.setAdapter(adapter);
        mAdView = new AdView(getActivity());
        mAdView.setAdUnitId(SessionManager.getAdBannerID());
        mAdView.setAdSize(AdSize.SMART_BANNER);
        ((RelativeLayout)view.findViewById(R.id.adView)).addView(mAdView);
        AdRequest adrequest = (new com.google.android.gms.ads.AdRequest.Builder()).addTestDevice(AdRequest.DEVICE_ID_EMULATOR).addTestDevice("5E1DFA3D59892F81D122FCFB60315D37").build();
        mAdView.loadAd(adrequest);
        return view;
    }

    public void onDestroy()
    {
        if (mAdView != null)
        {
            mAdView.destroy();
        }
        super.onDestroy();
    }

    public void onPause()
    {
        if (mAdView != null)
        {
            mAdView.pause();
        }
        super.onPause();
    }

    public void onResume()
    {
        super.onResume();
        if (mAdView != null)
        {
            mAdView.resume();
        }
    }

    @SuppressWarnings("unchecked")
	public void onStart()
    {
        super.onStart();
        (new FetchInfoTask(getActivity(), new GetTotalPlacesCallback() 
        {

            public void done(int i)
            {
                adapter.setTotalPlaces(i);
                adapter.notifyDataSetChanged();
            }
        })).execute(new String[0]);
        FetchInfoTask fetchinfotask;
        String as[];
        for (Iterator iterator = adapter.getSyncInfoList().iterator(); iterator.hasNext(); fetchinfotask.execute(as))
        {
            final SyncInfo info = (SyncInfo)iterator.next();
            fetchinfotask = new FetchInfoTask(getActivity(), new GetTotalPlacesCallback() 
            {
                public void done(int i)
                {
                    info.setTotalPlaces(i);
                    adapter.notifyDataSetChanged();
                }
            });
            as = new String[2];
            as[0] = info.getCity();
            as[1] = info.getCountry();
        }

    }
}