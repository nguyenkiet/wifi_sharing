package com.kietnt.freewifi.layout.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.wifi.ScanResult;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.LevenshteinDistance;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.model.PasswordItem;
import com.kietnt.freewifi.model.PasswordSecurity;
import com.kietnt.freewifi.model.Place;
import com.kietnt.freewifi.model.Vote;

import java.util.Date;
import java.util.List;

public class PlaceSingleAdapter extends BaseAdapter
{

    private Activity activity;
    private Vote currentVote;
    private LayoutInflater inflater;
    private Place place;
    private boolean isActive;

    public PlaceSingleAdapter(Activity activity1, Place place1, boolean active)
    {
    	isActive = active;
        activity = activity1;
        place = place1;
        currentVote = DatabaseHelper.getInstance(activity).getVoteByPlaceID(place1.getParseID());
        place.sortPassList();
    }

    private boolean havePermission()
    {
        String s = ConnectionManager.getInstance(activity).getConnectedWiFi();
        if (s == null)
        {
            (new android.app.AlertDialog.Builder(activity)).setTitle(R.string.Not_connected).setMessage(R.string.You_have_to_connect).setNegativeButton(R.string.OK, null).create().show();
            return false;
        }
        if (LevenshteinDistance.similarity(s, place.getName()) < 0.5D)
        {
            (new android.app.AlertDialog.Builder(activity)).setTitle(R.string.Not_connected).setMessage(R.string.you_are_connected_to_wrong_wifi).setNegativeButton(R.string.OK, null).create().show();
            return false;
        }
        if (!ConnectionManager.getInstance(activity).locationServiceEnabled())
        {
            (new android.app.AlertDialog.Builder(activity)).setTitle(R.string.GPS_is_OFF).setMessage(R.string.You_have_to_enable_GPS).setNegativeButton(R.string.OK, null).create().show();
            return false;
        }
        if (!SessionManager.getInstance().canVoteNow())
        {
            (new android.app.AlertDialog.Builder(activity)).setTitle(R.string.Please_wait).setMessage(R.string.To_prevent_spam).setNegativeButton(R.string.OK, null).create().show();
            return false;
        } else
        {
            return true;
        }
    }

    private boolean isConnected(Context context, String s, PasswordItem passworditem)
    {
        Log.d("wifi", (new StringBuilder()).append("check connection to ").append(s).toString());
        String s1 = ConnectionManager.getInstance(context).getConnectedWiFi();
        if (s1 != null)
        {
            Log.d("wifi", (new StringBuilder()).append("connected to ").append(s1).toString());
            if (s1.equalsIgnoreCase(s))
            {
                return true;
            }
        }
        return false;
    }

    private void shareWiFi(final Context context, String s, boolean voting)
    {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage(context.getString(R.string.Sending));
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(0);
        progressDialog.setCancelable(false);
        progressDialog.show();
        Log.d("parse", "send new pass");
        place.sendNewPassword(context, place.getParseID(), s, place, voting, false, new com.kietnt.freewifi.model.Place.OnSendNewPasswordCallback() 
        {
            public void done(boolean flag, Vote vote)
            {
                if (flag)
                {
                    if (vote != null)
                    {
                        DatabaseHelper.getInstance(context).addVote(vote);
                    }
                    progressDialog.dismiss();
                    Toast.makeText(context, context.getResources().getString(R.string.Send_WiFi_succeed), 0).show();
                    return;
                } else
                {
                    progressDialog.dismiss();
                    Toast.makeText(context, context.getResources().getString(R.string.Send_WiFi_failed_please_try_again), 0).show();
                    return;
                }
            }
        });
    }

    private void ConnectToWifi(final Context context, final ScanResult wifi)
    {
    	final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage((new StringBuilder()).append(context.getString(R.string.connect_wifi)).append(" ").append(wifi.SSID).toString());
        progressDialog.setIndeterminate(false);
        progressDialog.setProgressStyle(0);
        progressDialog.setCancelable(false);
        progressDialog.show();
        for (int j = 0; j < place.getPassList().size(); j++)
        {
            final PasswordItem item = (PasswordItem)place.getPassList().get(j);
            final int k = j;
            (new Handler()).postDelayed(new Runnable() 
            {

                public void run()
                {
                    if (isConnected(context, wifi.SSID, item))
                    {
                        progressDialog.dismiss();
                        Toast.makeText(context, context.getResources().getString(R.string.connected_wifi), 0).show();
                    } else
                    {
                        ConnectionManager.getInstance(context).connectWiFi(wifi, PasswordSecurity.decodedPassword(item.getPassword()));
                        if (k == -1 + place.getPassList().size())
                        {
                            (new Handler()).postDelayed(new Runnable() 
                            {
                                public void run()
                                {
                                    progressDialog.dismiss();
                                    if (isConnected(context, wifi.SSID, item))
                                    {
                                        Toast.makeText(context, context.getResources().getString(R.string.connected_wifi), 0).show();
                                        return;
                                    } else
                                    {
                                        Toast.makeText(context, context.getResources().getString(R.string.connect_wifi_fail), 0).show();
                                        return;
                                    }
                                }
                            }, 2000L);
                            return;
                        }
                    }
                } 
            }, 2000 * (j + 1));
        }
    }
    private void showWiFiSelection(final Context context, final List wifiList)
    {
        CharSequence acharsequence[] = new CharSequence[wifiList.size()];
        for (int i = 0; i < wifiList.size(); i++)
        {
            acharsequence[i] = ((ScanResult)wifiList.get(i)).SSID;
        }

        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);
        builder.setTitle(R.string.WiFi_list).setItems(acharsequence, new android.content.DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialoginterface, final int i)
            {
            	ConnectToWifi(context, (ScanResult)wifiList.get(i));
            } 
        })
        .setNegativeButton(R.string.Dont_Share, new android.content.DialogInterface.OnClickListener() 
        {
 
            public void onClick(DialogInterface dialoginterface, int j)
            {
                dialoginterface.dismiss();
            } 
        });
        builder.create().show();
    }

    public int getCount()
    {
        return 3 + place.getPassList().size();
    }

    public Object getItem(int i)
    {
        if (i < 2)
        {
            return null;
        } 
        else if(i<getCount() - 1)
        {
        	return (PasswordItem)place.getPassList().get(i - 2);
        }
        return null;
    }

    public long getItemId(int i)
    {
        return (long)(i - 2);
    }

    private GoogleMap map;
    private View viewMap;
    
    public View getView(int i, View view, final ViewGroup viewgroup)
    { 
        if (inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService("layout_inflater");
        }
        if (i == 0)
        {
            View view3 = inflater.inflate(R.layout.place_single_info, null);
            TextView textview3 = (TextView)view3.findViewById(R.id.placeSingleName);
            TextView textview4 = (TextView)view3.findViewById(R.id.placeSingleAddress);
            final Button btnHotPass = (Button)view3.findViewById(R.id.btnPlaceHotPassword);
            final Button btnWrongPass = (Button)view3.findViewById(R.id.btnWrongWifi);
            textview3.setText(place.getName());
            textview4.setText(place.getAddress());
            btnHotPass.setText(PasswordSecurity.decodedPassword(place.getPassword()));
            btnHotPass.setOnClickListener(new android.view.View.OnClickListener() 
            {

                public void onClick(View view4)
                {
                    Log.d("action", (new StringBuilder()).append("copy password ").append(btnHotPass.getText()).toString());
                    ((ClipboardManager)view4.getContext().getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("password", btnHotPass.getText()));
							Toast.makeText(
									view4.getContext(),
									view4.getContext()
											.getResources()
											.getString(R.string.copied_password), 0).show();
                }
            });
            // Wrong password click
            btnWrongPass.setOnClickListener(new android.view.View.OnClickListener() 
            {
                public void onClick(final View view4)
                {
                	if (havePermission())
                    {                        
                		String title = view4.getContext().getString(R.string.confirm_wrong_wifi_title);
                    	String message = view4.getContext().getString(R.string.confirm_wrong_wifi);                    	 
                        (new android.app.AlertDialog.Builder(view4.getContext())).setTitle(title).setMessage(message).setPositiveButton("Yes", new android.content.DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                            	String message = view4.getContext().getString(R.string.Sending);
                            	final ProgressDialog progressdialog = new ProgressDialog(view4.getContext());
                                progressdialog.setMessage(message);
                                progressdialog.setIndeterminate(false);
                                progressdialog.setProgressStyle(0);
                                progressdialog.setCancelable(false);
                                progressdialog.show();
                                place.setFailCount(place.getFailCount() + 1);
                                if(place.getFailCount() >= SessionManager.MAX_WRONG_NOTIFY)
                                {
                                	place.deletePlace(view4.getContext(), new com.kietnt.freewifi.model.Place.OnUpdateCallback()
                                    { 
                                        public void done(boolean flag)
                                        {
                                        	SessionManager.getInstance().setLastVoteTime(new Date());
                                        	DatabaseHelper.getInstance(view4.getContext()).deletePlace(place);
                                        	progressdialog.dismiss();
                                        } 
                                    });
                                }
                                else
                                {
                                	place.updatePlace(view4.getContext(), new com.kietnt.freewifi.model.Place.OnUpdateCallback()
                                    { 
                                        public void done(boolean flag)
                                        {
                                        	SessionManager.getInstance().setLastVoteTime(new Date());
                                        	progressdialog.dismiss();
                                        } 
                                    });
                                } 
                            } 
                        }).setNegativeButton("No", new android.content.DialogInterface.OnClickListener() 
                        {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                dialoginterface.cancel();
                            } 
                        }).create().show();
                    }
                }
            });
            return view3;
        }
        if (i == 1)
        {
            View view2 = inflater.inflate(R.layout.place_single_action, null);
            ((Button)view2.findViewById(R.id.btnConnect)).setOnClickListener(new android.view.View.OnClickListener() 
            {

                public void onClick(final View view4)
                {
                	String title = "K\u1EBFt n\u1ED1i Wi-Fi";
                	String message = view4.getContext().getString(R.string.confirm_connect_out);
                	if(isActive)
                	{
                		message = view4.getContext().getString(R.string.confirm_connect_in);
                	}
                    (new android.app.AlertDialog.Builder(view4.getContext())).setTitle(title).setMessage(message).setPositiveButton("K\u1EBFt n\u1ED1i ngay", new android.content.DialogInterface.OnClickListener() {

                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            Log.d("wifi", "connect wifi");
                            dialoginterface.dismiss();
                            String message = view4.getContext().getString(R.string.finding_wifi);
                            if(isActive) message = view4.getContext().getString(R.string.connect_wifi)+ " " + place.getName();
                            ConnectionManager connectionmanager = ConnectionManager.getInstance(view4.getContext());
                            final ProgressDialog progressdialog = new ProgressDialog(view4.getContext());
                            progressdialog.setMessage(message);
                            progressdialog.setIndeterminate(false);
                            progressdialog.setProgressStyle(0);
                            progressdialog.setCancelable(false);
                            progressdialog.show();
                            connectionmanager.scanWiFi(new com.kietnt.freewifi.helper.ConnectionManager.GetWiFiResult() 
                            { 
                                public void done(List list)
                                {
                                	progressdialog.dismiss();
                                	if(isActive)
                                	{
                                		String wifiName = place.getName();
                                		ScanResult itm = null;
                                		for(int i =0 ; i<list.size(); i++)
                                		{
                                			itm = (ScanResult) list.get(i);
                                			if(wifiName.equals(itm.SSID))
                                			{
                                				ConnectToWifi(view4.getContext(), itm);
                                				return;
                                			}
                                		}
                                	}
                                    showWiFiSelection(view4.getContext(), list);
                                } 
                            });
                        } 
                    }).setNegativeButton("B\u1ECF qua", new android.content.DialogInterface.OnClickListener() {
 

                        public void onClick(DialogInterface dialoginterface, int i)
                        {
                            dialoginterface.cancel();
                        } 
                    }).create().show();
                } 
            });
            ((Button)view2.findViewById(R.id.btnSendPwd)).setOnClickListener(new android.view.View.OnClickListener() 
            {
                public void onClick(final View view)
                {
                    if (havePermission())
                    {
                        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(view.getContext());
                        View view4 = ((LayoutInflater)view.getContext().getApplicationContext().getSystemService("layout_inflater")).inflate(R.layout.wifi_sharing, viewgroup, false);
                        TextView textview5 = (TextView)view4.findViewById(R.id.txtWiFiName);
                        final EditText edittext = (EditText)view4.findViewById(R.id.txtPassword);
                        textview5.setText(place.getName());
                        builder.setView(view4).setPositiveButton(R.string.Send, new android.content.DialogInterface.OnClickListener() 
                        {

                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                ((InputMethodManager)view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                                dialoginterface.dismiss();
                                if (edittext.getText().toString().equals(""))
                                {
                                    Toast.makeText(activity, activity.getResources().getString(R.string.Please_enter_WiFi_password), 0).show();
                                    return;
                                } else
                                {
                                    shareWiFi(view.getContext(), edittext.getText().toString(), false);
                                    return;
                                }
                            } 
                        }).setNegativeButton(R.string.Dont_Share, new android.content.DialogInterface.OnClickListener() 
                        {
                            public void onClick(DialogInterface dialoginterface, int i)
                            {
                                ((InputMethodManager)view.getContext().getSystemService("input_method")).hideSoftInputFromWindow(edittext.getWindowToken(), 0);
                                dialoginterface.dismiss();
                            } 
                        });
                        builder.create().show();
                        ((InputMethodManager)view.getContext().getSystemService("input_method")).toggleSoftInput(2, 0);
                    }
                } 
            });
            return view2;
        }
        if (i == getCount() - 1)
        {
        	// Load map for this place here
        	if(viewMap != null)
        	{
        		return viewMap;
        		/*
	        	ViewGroup viewgroup1 = (ViewGroup)viewMap.getParent();
	            if (viewgroup1 != null) viewgroup1.removeView(viewMap);
	            */
        	}
        	viewMap = inflater.inflate(R.layout.place_single_map, viewgroup, false);
	        map = ((MapFragment) activity.getFragmentManager().findFragmentById(R.id.singleMapFragment)).getMap();
	        if (map != null)
	        {
                com.google.android.gms.maps.CameraUpdate cameraupdate;
	        	map.setMyLocationEnabled(true); 
	        	map.addMarker((new MarkerOptions()).position(place.getLatLng()).title(place.getName()).icon(BitmapDescriptorFactory.fromResource(R.drawable.pin)));
	        	cameraupdate = CameraUpdateFactory.newCameraPosition((new com.google.android.gms.maps.model.CameraPosition.Builder()).target(new LatLng(place.getLatitude(), place.getLongitude())).zoom(14F).build());
	        	map.animateCamera(cameraupdate);
	        }
	        return viewMap;
        }
        View listVote = inflater.inflate(R.layout.place_single_pass_item, null);        
        TextView textview = (TextView)listVote.findViewById(R.id.place_single_password);
        TextView textview1 = (TextView)listVote.findViewById(R.id.place_single_password_votecount);
        TextView textview2 = (TextView)listVote.findViewById(R.id.place_single_password_date);
        final Button btnVote = (Button)listVote.findViewById(R.id.btnVote);
        final Button btnDeVote = (Button)listVote.findViewById(R.id.btnDeVote);
        
        final PasswordItem pwd = (PasswordItem)place.getPassList().get(i - 2);
        Gson gson = new Gson();
        Log.e("kietnt.8x", gson.toJson(pwd));
        textview.setText(PasswordSecurity.decodedPassword(pwd.getPassword()));
        textview1.setText(pwd.getVoteCountStr());
        textview2.setText(pwd.getDateDescription());
        if (currentVote != null)
        {
            if (pwd.getPassword().equals(currentVote.getPassword()))
            {
                btnVote.setBackgroundResource(R.drawable.liked);
            } 
            else
            {
                btnVote.setBackgroundResource(R.drawable.like);
            }
        }
        btnVote.setOnClickListener(new android.view.View.OnClickListener() 
        { 
            public void onClick(final View view)
            {
            	updateVote(view.getContext(), btnVote, place, true, false, pwd);
            } 
        });
        
        btnDeVote.setOnClickListener(new android.view.View.OnClickListener() 
        { 
            public void onClick(final View view)
            {
            	String title = view.getContext().getString(R.string.confirm_pwd_wrong_out_title);
            	String message = view.getContext().getString(R.string.confirm_pwd_wrong_out);
                (new android.app.AlertDialog.Builder(view.getContext())).setTitle(title).setMessage(message).setPositiveButton("Yes", new android.content.DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialoginterface, int i)
                    {
                    	updateVote(view.getContext(), btnDeVote, place, true, true, pwd);
                    } 
                }).setNegativeButton("No", new android.content.DialogInterface.OnClickListener() 
                { 
                    public void onClick(DialogInterface dialoginterface, int i)
                    {
                        dialoginterface.cancel();
                    } 
                }).create().show();            	
            } 
        });        
        return listVote;  
    }
    
    private void updateVote(final Context context, final Button parent, Place place, boolean voting, final boolean deVote, PasswordItem pwd)
    {
    	if (havePermission())
        {
            final ProgressDialog progressdialog = new ProgressDialog(context);
            progressdialog.setMessage(context.getString(R.string.Sending));
            progressdialog.setIndeterminate(false);
            progressdialog.setProgressStyle(0);
            progressdialog.setCancelable(false);
            progressdialog.show(); 
            place.sendNewPassword(context, place.getParseID(), PasswordSecurity.decodedPassword(pwd.getPassword()), place, voting, deVote, new com.kietnt.freewifi.model.Place.OnSendNewPasswordCallback() 
            { 
                public void done(boolean flag, Vote vote)
                {
                    if (flag)
                    {
                    	if(deVote)
                    	{
                    		parent.setBackgroundResource(R.drawable.disliked);
                    	}
                    	else
                    	{
                    		parent.setBackgroundResource(R.drawable.liked);
                    	}
                        if (vote != null)
                        {
                        	if(vote.getRemoved())
                        	{
                        		DatabaseHelper.getInstance(context).deleteVote(vote);
                        	}
                        	else
                        	{
	                            DatabaseHelper.getInstance(context).addVote(vote);
	                            currentVote = vote;
                        	}
                        }
                    }
                    progressdialog.dismiss();
                } 
            });
        }
    }
}