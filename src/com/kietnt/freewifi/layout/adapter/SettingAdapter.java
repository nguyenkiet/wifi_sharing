package com.kietnt.freewifi.layout.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.model.SyncInfo;
import java.util.List;

public class SettingAdapter extends BaseAdapter
{

    private Activity activity;
    private LayoutInflater inflater;
    private List syncInfoList;
    private int totalPlaces;

    public SettingAdapter(Activity activity1)
    {
        activity = activity1;
        syncInfoList = SessionManager.getInstance().getLocationSync();
        totalPlaces = -1;
    }

    public int getCount()
    {
        return 2 + syncInfoList.size();
    }

    public Object getItem(int i)
    {
        if (i < 2)
        {
            return null;
        } else
        {
            return (SyncInfo)syncInfoList.get(i - 2);
        }
    }

    public long getItemId(int i)
    {
        if (i < 2)
        {
            return -1L;
        } else
        {
            return (long)(i - 2);
        }
    }

    public List getSyncInfoList()
    {
        return syncInfoList;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService("layout_inflater");
        }
        if (i == 0)
        {
            View view3 = inflater.inflate(R.layout.setting_auto_update, null);
            Switch switch1 = (Switch)view3.findViewById(R.id.switchAutoUpdate);
            switch1.setChecked(SessionManager.getInstance().getAutoSync());
            switch1.setOnCheckedChangeListener(new android.widget.CompoundButton.OnCheckedChangeListener() 
            {
                public void onCheckedChanged(CompoundButton compoundbutton, boolean flag)
                {
                    SessionManager.getInstance().setAutoSync(flag);
                }
            });
            return view3;
        }
        if (i == 1)
        {
            View view2 = inflater.inflate(R.layout.setting_total_place, null);
            TextView textview2 = (TextView)view2.findViewById(R.id.txtTotalPlaces);
            if (totalPlaces == -1)
            {
                textview2.setText("...");
                return view2;
            } else
            {
                textview2.setText((new StringBuilder()).append(totalPlaces).append("").toString());
                return view2;
            }
        }
        View view1 = inflater.inflate(R.layout.setting_places_in_city, null);
        TextView textview = (TextView)view1.findViewById(R.id.txtCityName);
        TextView textview1 = (TextView)view1.findViewById(R.id.txtCityTotal);
        Button button = (Button)view1.findViewById(R.id.btnDeleteCity);
        final SyncInfo info = (SyncInfo)syncInfoList.get(i - 2);
        textview.setText((new StringBuilder()).append(info.getCity()).append(", ").append(info.getCountry()).toString());
        if (info.getTotalPlaces() == -1)
        {
            textview1.setText("...");
        } else
        {
            textview1.setText((new StringBuilder()).append(info.getTotalPlaces()).append("").toString());
        }
        button.setOnClickListener(new android.view.View.OnClickListener() {
            public void onClick(final View view)
            {
                final ProgressDialog progressdialog = new ProgressDialog(view.getContext());
                progressdialog.setMessage(view.getContext().getString(R.string.Deleting));
                progressdialog.setIndeterminate(false);
                progressdialog.setProgressStyle(0);
                progressdialog.setCancelable(false);
                progressdialog.show();
                (new Handler()).postDelayed(new Runnable() 
                {
                    public void run()
                    {
                        DatabaseHelper.getInstance(view.getContext()).deletePlaces(info.getCity(), info.getCountry());
                        syncInfoList.remove(info);
                        SessionManager.getInstance().saveLocationSync();
                        progressdialog.dismiss();
                        SettingAdapter.this.notifyDataSetChanged();
                    }
                }, 500L);
            }
        });
        return view1;
    }

    public void setSyncInfoList(List list)
    {
        syncInfoList = list;
    }

    public void setTotalPlaces(int i)
    {
        totalPlaces = i;
    }

}