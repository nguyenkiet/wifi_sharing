package com.kietnt.freewifi.layout.adapter;

import android.app.Activity;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.model.PasswordSecurity;
import com.kietnt.freewifi.model.Place;
import java.util.List;

public class PlaceListAdapter extends BaseAdapter
{

    private Activity activity;
    private LayoutInflater inflater;
    private List placeList;
    private List listWifi = null;

    public PlaceListAdapter(Activity activity1, List list)
    {
    	listWifi = null;
        activity = activity1;
        placeList = list;
    }
    public void setListWifi(List list)
    {
    	listWifi = list;
    }
    public int getCount()
    {
        return placeList.size();
    }

    public Object getItem(int i)
    {
        return placeList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public List getPlaceList()
    {
        return placeList;
    }
    
    public static int getSinalLevelResourceId(int level)
    {
    	int result = R.drawable.signal_1;
    	if(level == 1) result = R.drawable.signal_1;
    	else if(level == 2) result = R.drawable.signal_2;
    	else if(level == 3) result = R.drawable.signal_3;
    	else if(level >= 4) result = R.drawable.signal_4; 	
    	return result;
    }
    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService("layout_inflater");
        }
        if (view == null)
        {
            view = inflater.inflate(R.layout.place_list_item, null);
        }
        view.setBackground(view.getResources().getDrawable(R.drawable.bacgroundwhiterounded));
        TextView textview = (TextView)view.findViewById(R.id.placeName);
        TextView textview1 = (TextView)view.findViewById(R.id.placeAddress);
        TextView textview2 = (TextView)view.findViewById(R.id.placePassword);
        TextView acText = (TextView) view.findViewById(R.id.itemIsActive);
        ImageView signalLevel = (ImageView) view.findViewById(R.id.wifiSignalImgView);
        acText.setText("");
        signalLevel.setVisibility(View.GONE);
        Place place = (Place)placeList.get(i);
        textview.setText(place.getName());
        textview1.setText(place.getAddress());
        textview2.setText(PasswordSecurity.decodedPassword(place.getPassword()));
        if(listWifi != null)
        {
	        for (int j = 0; j < listWifi.size(); j++)
	        {
	        	ScanResult res = ((ScanResult)listWifi.get(j));       		
				if(place.getName().equals(res.SSID))
				{
					int strensgth = ConnectionManager.calculateWifiSingalStrength(res.level * -1);
					signalLevel.setVisibility(View.VISIBLE);
					signalLevel.setBackgroundResource(getSinalLevelResourceId(strensgth)); 
					view.setBackground(view.getResources().getDrawable(R.drawable.bacgroundwhiterounded_active));	                           	
	           	 	acText.setText("active");                      
				}
	        }
        }
        return view;
    }

    public void setPlaceList(List list)
    {
        placeList = list;
    }
}