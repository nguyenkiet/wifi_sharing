package com.kietnt.freewifi.layout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kietnt.freewifi.R;
import com.kietnt.freewifi.layout.model.NavDrawerItem;
import java.util.ArrayList;

public class NavDrawerListAdapter extends BaseAdapter
{

    private Context context;
    private ArrayList navDrawerItems;

    public NavDrawerListAdapter(Context context1, ArrayList arraylist)
    {
        context = context1;
        navDrawerItems = arraylist;
    }

    public int getCount()
    {
        return navDrawerItems.size();
    }

    public Object getItem(int i)
    {
        return navDrawerItems.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (view == null)
        {
            view = ((LayoutInflater)context.getSystemService("layout_inflater")).inflate(R.layout.drawer_list_item, null);
        }
        ImageView imageview = (ImageView)view.findViewById(R.id.icon);
        TextView textview = (TextView)view.findViewById(R.id.title);
        TextView textview1 = (TextView)view.findViewById(R.id.counter);
        imageview.setImageResource(((NavDrawerItem)navDrawerItems.get(i)).getIcon());
        textview.setText(((NavDrawerItem)navDrawerItems.get(i)).getTitle());
        if (((NavDrawerItem)navDrawerItems.get(i)).getCounterVisibility())
        {
            textview1.setText(((NavDrawerItem)navDrawerItems.get(i)).getCount());
            return view;
        } else
        {
            textview1.setVisibility(8);
            return view;
        }
    }
}