package com.kietnt.freewifi.layout.adapter;

import android.app.Activity;
import android.location.Address;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;

import com.kietnt.freewifi.R;

public class SyncTravelAdapter extends BaseAdapter
{

    private Activity activity;
    private List addressList;
    private LayoutInflater inflater;

    public SyncTravelAdapter(Activity activity1)
    {
        activity = activity1;
        addressList = new ArrayList();
    }

    public int getCount()
    {
        return addressList.size();
    }

    public Object getItem(int i)
    {
        return addressList.get(i);
    }

    public long getItemId(int i)
    {
        return (long)i;
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        if (inflater == null)
        {
            inflater = (LayoutInflater)activity.getSystemService("layout_inflater");
        }
        if (view == null)
        {
            view = inflater.inflate(R.layout.sync_travel_item, null);
        }
        TextView textview = (TextView)view.findViewById(R.id.txtAddressInfo);
        Address address = (Address)addressList.get(i);
        Log.d("geocoder", (new StringBuilder()).append("address ").append(address.toString()).toString());
        if (address.getAdminArea() == null)
        {
            textview.setText((new StringBuilder()).append(address.getFeatureName()).append(", ").append(address.getCountryName()).toString());
            return view;
        } else
        {
            textview.setText((new StringBuilder()).append(address.getAdminArea()).append(", ").append(address.getCountryName()).toString());
            return view;
        }
    }

    public void setAddressList(List list)
    {
        addressList = list;
    }
}