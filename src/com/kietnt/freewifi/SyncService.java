
package com.kietnt.freewifi;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Intent;
import android.util.Log;
import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.DatabaseManager;
import com.kietnt.freewifi.helper.SessionManager;
import com.parse.ParseException;

import java.util.List;

public class SyncService extends IntentService
{

    private int NOTIFICATION_ID;
    private DatabaseHelper databaseHelper;
    private NotificationManager mNotificationManager;

    public SyncService()
    {
        super("WiFi Ch\371a Sync Service");
        NOTIFICATION_ID = 1;
    }

    private void showNotification()
    {
        mNotificationManager = (NotificationManager)getSystemService("notification");
        android.app.Notification notification = (new android.support.v4.app.NotificationCompat.Builder(this)).setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.Database_updating)).setSmallIcon(R.drawable.logo_small_white).build();
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    private void stopNotification()
    {
        if (mNotificationManager != null)
        {
            mNotificationManager.cancel(NOTIFICATION_ID);
        }
        android.app.Notification notification = (new android.support.v4.app.NotificationCompat.Builder(this)).setContentTitle(getString(R.string.app_name)).setContentText(getString(R.string.Database_updated)).setSmallIcon(R.drawable.logo_small_white).build();
        mNotificationManager.notify(NOTIFICATION_ID, notification);
    }

    public void onCreate()
    {
        super.onCreate();
    }

    public void onDestroy()
    {
        super.onDestroy();
    }

    protected void onHandleIntent(Intent intent)
    {
    	android.location.Location location = SessionManager.getInstance().getLastLocation();
        if (location != null)
        {
        	Log.d("service", "service started");
            SessionManager.createInstance(this);
            if (ConnectionManager.getInstance(this).getConnectedWiFi() != null)
            {
                Log.d("service", "get location info");
                String as[] = SessionManager.getLocationInfo(this, location);
                if (as != null)
                {
                	boolean flag;
                    Log.d("service", (new StringBuilder()).append("have location ").append(as[0]).append(", ").append(as[1]).toString());
                    if (!as[0].equals("") && !as[1].equals("") && SessionManager.getInstance().isNeedToSync(as[0], as[1]))
                    {
                        showNotification();
                        databaseHelper = new DatabaseHelper(this);
                        do
                        {
                            Log.d("service", "sync on progress");
                            List list = null;
							try {
								list = DatabaseManager.getInstance().fetchListPlace(as[0], as[1]);
							} 
							catch (ParseException e) 
							{
								e.printStackTrace();
							}                            
                            if (list != null)
                            {
                                if (list.size() != 0)
                                {
                                    databaseHelper.addPlaceList(list);
                                }
                                if (list.size() == 1000)
                                {
                                    flag = true;
                                } else
                                {
                                    flag = false;
                                }
                            } else
                            {
                                flag = false;
                            }
                        } while (flag);
                        Log.d("service", "sync completed!");
                        stopNotification();
                    }
                }
            }
            return;
        } 
        Log.d("service", "no location");
    }

    public void onStart(Intent intent, int i)
    {
        super.onStart(intent, i);
    }
}
