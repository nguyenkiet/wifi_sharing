package com.kietnt.freewifi;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.ConnectionManager;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.DatabaseManager;
import com.kietnt.freewifi.helper.SessionManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import java.util.List;

public class WelcomeActivity extends Activity
{

    private Button btnAction;
    private int currentState;
    private ImageView imageLaunch;
    private int imgResources[];
    private String launchActions[];
    private String launchMessages[];
    private String launchTitles[];
    private LocationClient locationClient;
    private LocationRequest locationRequest;
    private TextView txtLaunchMessage;
    private TextView txtTitle;

    public WelcomeActivity()
    {
    }

    private boolean isLocationServicesEnabled()
    {
        if (ConnectionManager.getInstance(this).locationServiceEnabled())
        {
            if (locationClient == null)
            {
                return false;
            } else
            {
                Log.d("location", "location is on");
                return true;
            }
        } else
        {
            Log.d("location", "location is off");
            (new android.app.AlertDialog.Builder(this)).setTitle(R.string.GPS_is_OFF).setMessage(R.string.Please_turn_on_GPS).setNegativeButton(R.string.i_will_active_GPS, new android.content.DialogInterface.OnClickListener() 
            {

                public void onClick(DialogInterface dialoginterface, int i)
                {
                }
            }).create().show();
            return false;
        }
    }

    private void moveCurrentState()
    {
        currentState = 1 + currentState;
        if (currentState <= 4)
        {
            imageLaunch.setBackgroundResource(imgResources[currentState]);
            txtTitle.setText(launchTitles[currentState]);
            txtLaunchMessage.setText(launchMessages[currentState]);
            btnAction.setText(launchActions[currentState]);
            return;
        } else
        {
            SessionManager.getInstance().setFirstTimeUsingApp(false);
            finish();
            return;
        }
    }

    private void updateDatabase(final Location currentLocation)
    {
        DatabaseManager.getInstance().fetchAllPlaceNearby(currentLocation, 0.5D, new com.kietnt.freewifi.helper.DatabaseManager.FindParseDataCallback()
        {
            public void done(boolean flag, List list)
            {
                if (flag)
                {
                    Log.d("parse", "sync completed!");
                    if (list.size() != 0)
                    {
                        DatabaseHelper.getInstance(getApplicationContext()).addPlaceList(list);
                    }
                }
                SessionManager.getInstance().setLastLocation(currentLocation);
            }
        });
    }

    public void onBackPressed()
    {
        Toast.makeText(this, R.string.you_must_continue, 0).show();
    }
 

    OnConnectionFailedListener onFailure =  new com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener() {		
		@Override
		public void onConnectionFailed(ConnectionResult arg0) {
			Log.d("location", "location failed");
		}
	};
	ConnectionCallbacks onSucessed = new ConnectionCallbacks() {
		
		public void onDisconnected() {
			Log.d("location", "location disconnected");
		}
		
		public void onConnected(Bundle arg0) {
			Log.d("location", "location connected");
	        if (locationClient.getLastLocation() == null)
	        {
	            locationClient.requestLocationUpdates(locationRequest, new LocationListener() 
	            {

	                public void onLocationChanged(Location location)
	                {
	                    Log.d("location", "location updated");
	                    if (locationClient.getLastLocation() != null)
	                    {
	                        locationClient.removeLocationUpdates(this);
	                        updateDatabase(locationClient.getLastLocation());
	                    }
	                } 
	            });
	            return;
	        } else
	        {
	            updateDatabase(locationClient.getLastLocation());
	            return;
	        }
		}
	};
    protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle); 
        setContentView(R.layout.launch);
        locationClient = new LocationClient(this, onSucessed, onFailure);
        locationRequest = new LocationRequest();
        locationRequest.setPriority(100);
        locationRequest.setInterval(5L);
        locationRequest.setFastestInterval(1L);
        imageLaunch = (ImageView)findViewById(R.id.imageLaunch);
        txtTitle = (TextView)findViewById(R.id.txtTitle);
        txtLaunchMessage = (TextView)findViewById(R.id.txtLaunchMessage);
        btnAction = (Button)findViewById(R.id.btnAction);
        String as[] = new String[5];
        as[0] = getString(R.string.Hello);
        as[1] = getString(R.string.Share_Location);
        as[2] = getString(R.string.Warning);
        as[3] = getString(R.string.Auto_Update);
        as[4] = getString(R.string.Thanks);
        launchTitles = as;
        String as1[] = new String[5];
        as1[0] = getString(R.string.Launch_Welcome);
        as1[1] = getString(R.string.Launch_Location);
        as1[2] = getString(R.string.Launch_Warning);
        as1[3] = getString(R.string.Launch_Auto_Update);
        as1[4] = getString(R.string.Launch_Thanks);
        launchMessages = as1;
        String as2[] = new String[5];
        as2[0] = getString(R.string.Continue);
        as2[1] = getString(R.string.Activate_Location);
        as2[2] = getString(R.string.Agree);
        as2[3] = getString(R.string.Enable_Notification);
        as2[4] = getString(R.string.Start);
        launchActions = as2;
        imgResources = (new int[] {
            R.drawable.launch_welcome, R.drawable.launch_location, R.drawable.launch_warning, R.drawable.launch_cloud, R.drawable.launch_heart
        });
        currentState = -1;
        moveCurrentState();
        btnAction.setOnClickListener(new android.view.View.OnClickListener() 
        {

            public void onClick(View view)
            {
                if (currentState == 1 || currentState == 3)
                {
                    if (isLocationServicesEnabled())
                    {
                        moveCurrentState();
                    }
                    return;
                } else
                {
                    moveCurrentState();
                    return;
                }
            }
        });
    }
 
    public void onStart()
    {
        super.onStart();
        locationClient.connect();
        Log.d("location", "start location");
    }

    public void onStop()
    {
        if (locationClient != null)
        {
            locationClient.disconnect();
        }
        super.onStop();
    }
}
