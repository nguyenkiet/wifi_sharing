package com.kietnt.freewifi.helper;

import android.location.Location;
import android.util.Log;
import com.kietnt.freewifi.model.Place;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

// Referenced classes of package com.kietnt.freewifi.helper:
//            SessionManager

public class DatabaseManager
{
    public static interface FindParseDataCallback
    {
        public abstract void done(boolean flag, List list);
    }


    private static DatabaseManager ourInstance = new DatabaseManager();

    private DatabaseManager()
    {
    }

    public static DatabaseManager getInstance()
    {
        DatabaseManager databasemanager = ourInstance;
        return databasemanager;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	public void fetchAllPlace(final String city, final String country, final FindParseDataCallback callback)
    {
        Log.d("parse", "fetching all place");
        ParseQuery parsequery = ParseQuery.getQuery("Place");
        parsequery.setLimit(1000);
        parsequery.orderByAscending("updatedAt");
        parsequery.whereEqualTo("city_ggl", city);
        parsequery.whereEqualTo("country", country);
        parsequery.whereGreaterThanOrEqualTo("updatedAt", SessionManager.getInstance().getLastSyncTime(city, country));
        parsequery.findInBackground(new FindCallback() 
        {
            public void done(List list, ParseException parseexception)
            {
                if (parseexception == null)
                {
                    Log.d("parse", (new StringBuilder()).append("fetched ").append(list.size()).append(" results").toString());
                    if (list.size() == 0)
                    {
                        callback.done(true, null);
                        return;
                    } 
                    ArrayList arraylist = new ArrayList();
                    for (Iterator iterator = list.iterator(); iterator.hasNext(); arraylist.add(new Place((ParseObject)iterator.next()))) { }
                    SessionManager.getInstance().setLastSyncTime(((ParseObject)list.get(-1 + list.size())).getUpdatedAt(), city, country);
                    Log.d("parse", "fetch completed");
                    callback.done(true, arraylist);
                    return;
                } else
                {
                    Log.d("parse", (new StringBuilder()).append("error: ").append(parseexception.getMessage()).toString());
                    callback.done(false, null);
                    return;
                }
            }
        });
    }

    @SuppressWarnings({ "unchecked", "rawtypes"})
	public void fetchAllPlaceNearby(Location location, double d, final FindParseDataCallback callback)
    {
        ParseQuery parsequery = ParseQuery.getQuery("Place");
        parsequery.setLimit(1000);
        parsequery.whereWithinKilometers("location", new ParseGeoPoint(location.getLatitude(), location.getLongitude()), d);
        parsequery.findInBackground(new FindCallback() 
        {
            public void done(List list, ParseException parseexception)
            {
                if (parseexception == null)
                {
                    ArrayList arraylist = new ArrayList();
                    for (Iterator iterator = list.iterator(); iterator.hasNext(); arraylist.add(new Place((ParseObject)iterator.next()))) { }
                    callback.done(true, arraylist);
                    return;
                } else
                {
                    Log.d("parse", (new StringBuilder()).append("error: ").append(parseexception.getMessage()).toString());
                    callback.done(false, null);
                    return;
                }
            } 
        });
    }

    @SuppressWarnings("unchecked")
	public List fetchListPlace(String s, String s1) throws ParseException
    {
        ParseQuery parsequery;
        Log.d("parse", "fetching all place");
        parsequery = ParseQuery.getQuery("Place");
        parsequery.setLimit(1000);
        parsequery.orderByAscending("updatedAt");
        parsequery.whereEqualTo("city_ggl", s);
        parsequery.whereEqualTo("country", s1);
        parsequery.whereGreaterThanOrEqualTo("updatedAt", SessionManager.getInstance().getLastSyncTime(s, s1));
        List list = null;
		try 
		{
			list = parsequery.find();
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
        ArrayList arraylist;
        if (list.size() == 0)
        {
            return null;
        }
        arraylist = new ArrayList();
		for (Iterator iterator = list.iterator(); iterator.hasNext(); arraylist.add(new Place((ParseObject)iterator.next()))) { }
        SessionManager.getInstance().setLastSyncTime(((ParseObject)list.get(-1 + list.size())).getUpdatedAt(), s, s1);
        Log.d("parse", "fetch completed");
        return arraylist;
    }

    @SuppressWarnings("unchecked")
	public void fetchPlaceNearbyForSharing(Location location, double d, final FindParseDataCallback callback)
    {
        ParseQuery parsequery = ParseQuery.getQuery("Place");
        parsequery.setLimit(20);
        parsequery.orderByAscending("location");
        parsequery.whereWithinKilometers("location", new ParseGeoPoint(location.getLatitude(), location.getLongitude()), d);
        parsequery.findInBackground(new FindCallback() 
        {
            public void done(List list, ParseException parseexception)
            {
                if (parseexception == null)
                {
                    ArrayList arraylist = new ArrayList();
                    for (Iterator iterator = list.iterator(); iterator.hasNext(); arraylist.add(new Place((ParseObject)iterator.next()))) { }
                    callback.done(true, arraylist);
                    return;
                } else
                {
                    Log.d("parse", (new StringBuilder()).append("error: ").append(parseexception.getMessage()).toString());
                    callback.done(false, null);
                    return;
                }
            }
        });
    }
}