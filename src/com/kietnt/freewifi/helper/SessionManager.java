package com.kietnt.freewifi.helper;

import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;
import com.kietnt.freewifi.model.SyncInfo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class SessionManager
{
    public static interface GetLocationAddressCallback
    {

        public abstract void onComplete(boolean flag, String s, String s1, String s2);
    }

    private static class GetLocationAddressTask extends AsyncTask
    {

        private GetLocationAddressCallback mCallback;
        private Context mContext;

        @Override
        protected Object doInBackground(Object ...aobj)
        {
            return doInBackground((Location[])aobj);
        }

        protected Void doInBackground(Location alocation[])
        {
            Geocoder geocoder = new Geocoder(mContext, Locale.ENGLISH);
            Location location = alocation[0];
            Log.d("geocoder", (new StringBuilder()).append("location (").append(location.getLatitude()).append(",").append(location.getLongitude()).append(")").toString());
            List list;
            try
            {
                list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            }
            catch (IOException ioexception)
            {
                Log.e("geocoder", "IO Exception in getFromLocation()");
                ioexception.printStackTrace();
                mCallback.onComplete(false, null, null, null);
                return null;
            }
            catch (IllegalArgumentException illegalargumentexception)
            {
                Log.e("geocoder", (new StringBuilder()).append("Illegal arguments ").append(Double.toString(location.getLatitude())).append(" , ").append(Double.toString(location.getLongitude())).append(" passed to address service").toString());
                illegalargumentexception.printStackTrace();
                if (mCallback != null)
                {
                    mCallback.onComplete(false, null, null, null);
                }
                return null;
            }
            if (list != null && list.size() > 0)
            {
                Address address = (Address)list.get(0);
                String s = "";
                for (int i = 0; i < address.getMaxAddressLineIndex(); i++)
                {
                    s = (new StringBuilder()).append(s).append(address.getAddressLine(i)).append(" ").toString();
                }

                String s1 = address.getCountryName();
                if (s1.equals("Vi\u1EC7t Nam"))
                {
                    s1 = "Vietnam";
                }
                Log.d("geocoder", (new StringBuilder()).append("address : ").append(s).append(" / state : ").append(address.getAdminArea()).append(" / country : ").append(s1).toString());
                mCallback.onComplete(true, s, address.getAdminArea(), s1);
            } else
            {
                mCallback.onComplete(false, null, null, null);
            }
            return null;
        }

        public GetLocationAddressTask(Context context, GetLocationAddressCallback getlocationaddresscallback)
        {
            mContext = context;
            mCallback = getlocationaddresscallback;
        }
    }


    public static int SYNC_CHECK_TIME = 10800;
    public static int MAX_WRONG_NOTIFY = 10;
    private static SessionManager ourInstance;
    private int ANTI_SPAM_SHARE;
    private int ANTI_SPAM_VOTE;
    private int SYNC_RANGE_TIME;
    private android.content.SharedPreferences.Editor editor;
    private Gson gson;
    private List<SyncInfo> locationSync;
    private Context mContext;
    private SharedPreferences sharedPreferences;

    @SuppressWarnings("unchecked")
	private SessionManager(Context context)
    {
        ANTI_SPAM_VOTE = 120;
        ANTI_SPAM_SHARE = 600;
        SYNC_RANGE_TIME = 0x2a300;
        mContext = context;
        sharedPreferences = context.getSharedPreferences("freewifi2015", 0);
        editor = sharedPreferences.edit();
        gson = new Gson();
        locationSync = new ArrayList<SyncInfo>();
        String s;
        try
        {
            s = sharedPreferences.getString("locationSync", null);
        }
        catch (Exception exception)
        {
            return;
        }
        if (s == null)
        {
            return; 
        }
        locationSync = (List<SyncInfo>) gson.fromJson(s, new TypeToken<List<SyncInfo>>() {}.getType());
    }

    public static void createInstance(Context context)
    {
        if (ourInstance == null)
        {
            ourInstance = new SessionManager(context);
        }
    }

    public static String getAdBannerID()
    {
        return "ca-app-pub-4165324413119882/9086410328";
    }

    public static SessionManager getInstance()
    {
        return ourInstance;
    }

    public static String getInterstitialAdID()
    {
        return "ca-app-pub-4165324413119882/4512949921";
    }

    @SuppressWarnings("unchecked")
	public static void getLocationInfo(Context context, Location location, GetLocationAddressCallback getlocationaddresscallback)
    {
        (new GetLocationAddressTask(context, getlocationaddresscallback)).execute(new Location[] {
            location
        });
    }

    public static String[] getLocationInfo(Context context, Location location)
    {
        Geocoder geocoder;
        String s = "";
        geocoder = new Geocoder(context, Locale.ENGLISH);
        Log.d("geocoder", (new StringBuilder()).append("location (").append(location.getLatitude()).append(",").append(location.getLongitude()).append(")").toString());
        List list = null;
		try {
			list = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
		} catch (IOException e) { 
			e.printStackTrace();
		}
        if (list == null)
        {
        	return null;
        }
        else
        {
        	if (list.size() == 0)
        	{
        		return null;
        	}
        	else
        	{
        		Address address = (Address)list.get(0);
                if (address.getAdminArea() != null) 
            	{
                	s = address.getAdminArea();
                	if (address.getCountryName() != null) 
                	{
                		String s2 = address.getCountryName();
                        if (s2.equals("Vi\u1EC7t Nam"))
                        {
                        	s2 = "Vietnam";
                        }
                        return (new String[] {
                            s, s2
                        });
                	}
                	else
                	{
                		s = "";
                	}
            	}
                else
                {
                	
                }
        	}
        }
        return (new String[] {
                s, ""
            });
    }

    public boolean canShareNow()
    {
        long l = Math.abs((getLastShareTime().getTime() - (new Date()).getTime()) / 1000L);
        Log.d("session", (new StringBuilder()).append("last share range ").append(l).toString());
        return l >= (long)ANTI_SPAM_SHARE;
    }

    public boolean canVoteNow()
    {
        long l = Math.abs((getLastVoteTime().getTime() - (new Date()).getTime()) / 1000L);
        Log.d("session", (new StringBuilder()).append("last vote range ").append(l).toString());
        return l >= (long)ANTI_SPAM_VOTE; // Kietnt
    }

    public boolean getAutoSync()
    {
        return sharedPreferences.getBoolean("autoSync", true);
    }

    public Location getLastLocation()
    {
        double d = sharedPreferences.getFloat("lastLatitude", 0.0F);
        double d1 = sharedPreferences.getFloat("lastLongitude", 0.0F);
        if (d == 0.0D)
        {
            return null;
        } else
        {
            Location location = new Location("");
            location.setLatitude(d);
            location.setLongitude(d1);
            return location;
        }
    }

    public Date getLastShareTime()
    {
        if (sharedPreferences.getString("lastShare", null) != null)
        {
            Log.d("session", (new StringBuilder()).append("last share ").append(((Date)gson.fromJson(sharedPreferences.getString("lastShare", null), Date.class)).toString()).toString());
            return (Date)gson.fromJson(sharedPreferences.getString("lastShare", null), Date.class);
        } else
        {
            return new Date(0L);
        }
    }

    public Date getLastSyncTime(String s, String s1)
    {
        for (Iterator iterator = locationSync.iterator(); iterator.hasNext();)
        {
            SyncInfo syncinfo = (SyncInfo)iterator.next();
            if (syncinfo.getCity().equals(s) && syncinfo.getCountry().equals(s1))
            {
                return syncinfo.getLastSync();
            }
        }

        return new Date(0L);
    }

    public Date getLastVoteTime()
    {
        if (sharedPreferences.getString("lastVote", null) != null)
        {
            Log.d("session", (new StringBuilder()).append("last vote ").append(((Date)gson.fromJson(sharedPreferences.getString("lastVote", null), Date.class)).toString()).toString());
            return (Date)gson.fromJson(sharedPreferences.getString("lastVote", null), Date.class);
        } else
        {
            return new Date(0L);
        }
    }

    public List getLocationSync()
    {
        return locationSync;
    }

    public int getTotalViewPlace()
    {
        int i = sharedPreferences.getInt("totalViewPlace", 1);
        Log.d("view", (new StringBuilder()).append("total view ").append(i).toString());
        return i;
    }

    public boolean haveRunningService()
    {
        for (Iterator iterator = ((ActivityManager)mContext.getSystemService("activity")).getRunningServices(Integer.MAX_VALUE).iterator(); iterator.hasNext();)
        {
            android.app.ActivityManager.RunningServiceInfo runningserviceinfo = (android.app.ActivityManager.RunningServiceInfo)iterator.next();
            Log.d("service", (new StringBuilder()).append("running ").append(runningserviceinfo.service.getClassName()).toString());
            if ("com.kietnt.freewifi.SyncService".equals(runningserviceinfo.service.getClassName()))
            {
                return true;
            }
        }

        return false;
    }

    public boolean isFirstTimeUsingApp()
    {
        return sharedPreferences.getBoolean("firstTime", true);
    }

    public boolean isNeedToSync(String s, String s1)
    {
        Date date = getLastSyncTime(s, s1);
        return Math.abs(((new Date()).getTime() - date.getTime()) / 1000L) > (long)SYNC_RANGE_TIME;
    }

    public boolean isShowAd()
    {
        return sharedPreferences.getBoolean("showGAD", true);
    }

    public void saveLocationSync()
    {
        String s = gson.toJson(locationSync);
        editor.putString("locationSync", s);
        editor.apply();
    }

    public void setAutoSync(boolean flag)
    {
        editor.putBoolean("autoSync", flag);
        editor.apply();
    }

    public void setFirstTimeUsingApp(boolean flag)
    {
        editor.putBoolean("firstTime", flag);
        editor.apply();
    }

    public void setIncreaseTotalViewPlace()
    {
        int i = 1 + getTotalViewPlace();
        editor.putInt("totalViewPlace", i);
        editor.apply();
    }

    public void setLastLocation(Location location)
    {
        editor.putFloat("lastLatitude", (float)location.getLatitude());
        editor.putFloat("lastLongitude", (float)location.getLongitude());
        editor.apply();
    }

    public void setLastShareTime(Date date)
    {
        Log.d("session", (new StringBuilder()).append("set last share ").append(date.toString()).toString());
        String s = gson.toJson(date);
        editor.putString("lastShare", s);
        editor.apply();
    }

    public void setLastSyncTime(Date date, String s, String s1)
    {
        for (Iterator iterator = locationSync.iterator(); iterator.hasNext();)
        {
            SyncInfo syncinfo1 = (SyncInfo)iterator.next();
            if (syncinfo1.getCity().equals(s) && syncinfo1.getCountry().equals(s1))
            {
                syncinfo1.setLastSync(date);
                saveLocationSync();
                return;
            }
        }

        SyncInfo syncinfo = new SyncInfo(s, s1, date);
        locationSync.add(syncinfo);
        saveLocationSync();
    }

    public void setLastVoteTime(Date date)
    {
        Log.d("session", (new StringBuilder()).append("set last vote ").append(date.toString()).toString());
        String s = gson.toJson(date);
        editor.putString("lastVote", s);
        editor.apply();
    }

    public void setShowAd(boolean flag)
    {
        editor.putBoolean("showGAD", flag);
        editor.apply();
    }

}