package com.kietnt.freewifi.helper;

import java.io.PrintStream;

public class LevenshteinDistance
{

    public LevenshteinDistance()
    {
    }

    public static int computeEditDistance(String s, String s1)
    {
        String s2 = s.toLowerCase();
        String s3 = s1.toLowerCase();
        int ai[] = new int[1 + s3.length()];
        for (int i = 0; i <= s2.length(); i++)
        {
            int j = i;
            int k = 0;
            while (k <= s3.length()) 
            {
                if (i == 0)
                {
                    ai[k] = k;
                } else
                if (k > 0)
                {
                    int l = ai[k - 1];
                    if (s2.charAt(i - 1) != s3.charAt(k - 1))
                    {
                        l = 1 + Math.min(Math.min(l, j), ai[k]);
                    }
                    ai[k - 1] = j;
                    j = l;
                }
                k++;
            }
            if (i > 0)
            {
                ai[s3.length()] = j;
            }
        }

        return ai[s3.length()];
    }

    public static void printDistance(String s, String s1)
    {
        System.out.println((new StringBuilder()).append(s).append("-->").append(s1).append(": ").append(computeEditDistance(s, s1)).append(" (").append(similarity(s, s1)).append(")").toString());
    }

    public static double similarity(String s, String s1)
    {
        if (s.length() < s1.length())
        {
            String s2 = s;
            s = s1;
            s1 = s2;
        }
        int i = s.length();
        if (i == 0)
        {
            return 1.0D;
        } else
        {
            return (double)(i - computeEditDistance(s, s1)) / (double)i;
        }
    }
}