package com.kietnt.freewifi.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;

import com.google.gson.Gson;
import com.kietnt.freewifi.model.Place;
import com.kietnt.freewifi.model.Vote;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper
{

    private static final String DATABASE_NAME = "database";
    private static final int DATABASE_VERSION = 1;
    private static final String KEY_PLACE_ADDRESS = "address";
    private static final String KEY_PLACE_CITY = "city";
    private static final String KEY_PLACE_COUNTRY = "country";
    private static final String KEY_PLACE_ID = "id";
    private static final String KEY_PLACE_LATITUDE = "latitude";
    private static final String KEY_PLACE_LONGITUDE = "longitude";
    private static final String KEY_PLACE_NAME = "name";
    private static final String KEY_PLACE_PASSLIST = "passList";
    private static final String KEY_PLACE_PASSWORD = "password";
    private static final String KEY_PLACE_FAIL_COUNT = "failCount";
    private static final String KEY_VOTE_ID = "id";
    private static final String KEY_VOTE_PASSWORD = "password";
    private static final String KEY_VOTE_PLACEID = "placeID";
    private static final String TABLE_PLACES = "place";
    private static final String TABLE_VOTES = "vote";
    private static DatabaseHelper mInstance;

    public DatabaseHelper(Context context)
    {
        super(context, "database", null, 1);
    }

    public static DatabaseHelper getInstance(Context context)
    {
        if (mInstance == null)
        {
            mInstance = new DatabaseHelper(context);
        }
        return mInstance;
    }

    public void addPlace(Place place)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("id", place.getParseID());
        contentvalues.put("name", place.getName());
        contentvalues.put("address", place.getAddress());
        contentvalues.put("password", place.getPassword());
        contentvalues.put("city", place.getCity());
        contentvalues.put("country", place.getCountry());
        contentvalues.put("latitude", Double.valueOf(place.getLatitude()));
        contentvalues.put("longitude", Double.valueOf(place.getLongitude()));
        Gson gson = new Gson();
        String json = gson.toJson(place.getPassList());
        contentvalues.put("passList", json);//place.getPassListStr());
        contentvalues.put("failCount", place.getFailCount());
        sqlitedatabase.insertWithOnConflict("place", null, contentvalues, 5);
        sqlitedatabase.close();
    }

    public void addPlaceList(List list)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        sqlitedatabase.beginTransactionNonExclusive();
        ContentValues contentvalues;
        for (Iterator iterator = list.iterator(); iterator.hasNext(); sqlitedatabase.insertWithOnConflict("place", null, contentvalues, 5))
        {
            Place place = (Place)iterator.next();
            contentvalues = new ContentValues();
            contentvalues.put("id", place.getParseID());
            contentvalues.put("name", place.getName());
            contentvalues.put("address", place.getAddress());
            contentvalues.put("password", place.getPassword());
            contentvalues.put("city", place.getCity());
            contentvalues.put("country", place.getCountry());
            contentvalues.put("latitude", Double.valueOf(place.getLatitude()));
            contentvalues.put("longitude", Double.valueOf(place.getLongitude()));
            Gson gson = new Gson();
            String json = gson.toJson(place.getPassList());
            contentvalues.put("passList", json);//place.getPassListStr());
            contentvalues.put("failCount", place.getFailCount());
        }

        sqlitedatabase.setTransactionSuccessful();
        sqlitedatabase.endTransaction();
        sqlitedatabase.close();
    }

    public void addVote(Vote vote)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("placeID", vote.getPlaceID());
        contentvalues.put("id", vote.getId());
        contentvalues.put("password", vote.getPassword());
        sqlitedatabase.insertWithOnConflict("vote", null, contentvalues, 5);
        sqlitedatabase.close();
    }

    public double degreeToRadians(double d)
    {
        return 3.1415926535897931D * (d / 180D);
    }

    public void deletePlace(Place place)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        String as[] = new String[1];
        as[0] = place.getParseID();
        sqlitedatabase.delete("place", "id=?", as);
        sqlitedatabase.close();
    }

    public void deletePlaces(String s, String s1)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        sqlitedatabase.delete("place", "city=? AND country=?", new String[] {
            s, s1
        });
        sqlitedatabase.close();
    }

    public void deleteVote(Vote vote)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        String as[] = new String[1];
        as[0] = vote.getPlaceID();
        sqlitedatabase.delete("vote", "placeID=?", as);
        sqlitedatabase.close();
    }

    public Place getPlace(String s)
    {
        Cursor cursor = getReadableDatabase().query("place", null, "id=?", new String[] {
            s
        }, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
        }
        Place place = new Place(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("password")), cursor.getString(cursor.getColumnIndex("city")), cursor.getString(cursor.getColumnIndex("country")), cursor.getDouble(cursor.getColumnIndex("latitude")), cursor.getDouble(cursor.getColumnIndex("longitude")), cursor.getString(cursor.getColumnIndex("passList")), cursor.getInt(cursor.getColumnIndex("failCount")));
        cursor.close();
        return place;
    }

    public List getPlacesAround(Location location, double d)
    {
        SQLiteDatabase sqlitedatabase = getReadableDatabase();
        double d1 = location.getLongitude() - d / Math.abs(69D * Math.cos(degreeToRadians(location.getLatitude())));
        double d2 = location.getLongitude() + d / Math.abs(69D * Math.cos(degreeToRadians(location.getLatitude())));
        double d3 = location.getLatitude() - d / 69D;
        double d4 = location.getLatitude() + d / 69D;
        String as[] = new String[4];
        StringBuilder stringbuilder = new StringBuilder();
        double d5;
        StringBuilder stringbuilder1;
        StringBuilder stringbuilder2;
        double d6;
        StringBuilder stringbuilder3;
        Cursor cursor;
        ArrayList arraylist;
        if (d4 > d3)
        {
            d5 = d3;
        } else
        {
            d5 = d4;
        }
        as[0] = stringbuilder.append(d5).append("").toString();
        stringbuilder1 = new StringBuilder();
        if (d4 <= d3)
        {
            d4 = d3;
        }
        as[1] = stringbuilder1.append(d4).append("").toString();
        stringbuilder2 = new StringBuilder();
        if (d2 > d1)
        {
            d6 = d1;
        } else
        {
            d6 = d2;
        }
        as[2] = stringbuilder2.append(d6).append("").toString();
        stringbuilder3 = new StringBuilder();
        if (d2 <= d1)
        {
            d2 = d1;
        }
        as[3] = stringbuilder3.append(d2).append("").toString();
        cursor = sqlitedatabase.query("place", null, "latitude >= ? AND latitude <= ? AND longitude >= ? AND longitude <= ?", as, null, null, null);
        arraylist = new ArrayList();
        if (cursor != null)
        {
            for (; cursor.moveToNext(); arraylist.add(new Place(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("password")), cursor.getString(cursor.getColumnIndex("city")), cursor.getString(cursor.getColumnIndex("country")), cursor.getDouble(cursor.getColumnIndex("latitude")), cursor.getDouble(cursor.getColumnIndex("longitude")), cursor.getString(cursor.getColumnIndex("passList")), cursor.getInt(cursor.getColumnIndex("failCount"))))) { }
            cursor.close();
        }
        int i = 0;
        do
        {
            int j = arraylist.size();
            if (i < j)
            {
                int k = i + 1;
                do
                {
                    int l = arraylist.size();
                    if (k >= l)
                    {
                        break;
                    }
                    Place place = (Place)arraylist.get(i);
                    Place place1 = (Place)arraylist.get(k);
                    if (location.distanceTo(place.getLocation()) > location.distanceTo(place1.getLocation()))
                    {
                        arraylist.set(i, place1);
                        arraylist.set(k, place);
                    }
                    k++;
                } while (true);
                i++;
            } else
            {
                return arraylist;
            }
        } while (true);
    }

    public long getTotalPlace()
    {
        return DatabaseUtils.queryNumEntries(getReadableDatabase(), "place");
    }

    public long getTotalPlace(String s, String s1)
    {
        return DatabaseUtils.queryNumEntries(getReadableDatabase(), "place", "city=? AND country=?", new String[] {
            s, s1
        });
    }

    public Vote getVoteByID(String s)
    {
        Cursor cursor = getReadableDatabase().query("vote", null, "id=?", new String[] {
            s
        }, null, null, null);
        if (cursor != null)
        {
            cursor.moveToFirst();
        }
        Vote vote = new Vote(cursor.getString(0), cursor.getString(1), cursor.getString(2));
        cursor.close();
        return vote;
    }

    public Vote getVoteByPlaceID(String s)
    {
        Cursor cursor = getReadableDatabase().query("vote", null, "placeID=?", new String[] {
            s
        }, null, null, null);
        if (cursor != null && cursor.moveToFirst())
        {
            Vote vote = new Vote(cursor.getString(0), cursor.getString(1), cursor.getString(2));
            cursor.close();
            return vote;
        } else
        {
            return null;
        }
    }

    public void onCreate(SQLiteDatabase sqlitedatabase)
    {
        sqlitedatabase.execSQL("CREATE TABLE place(id TEXT PRIMARY KEY,name TEXT,address TEXT,password TEXT,city TEXT,country TEXT,latitude DOUBLE,longitude DOUBLE,passList TEXT, failCount INT )");
        sqlitedatabase.execSQL("CREATE TABLE vote(placeID TEXT PRIMARY KEY,id TEXT,password TEXT )");
    }

    public void onUpgrade(SQLiteDatabase sqlitedatabase, int i, int j)
    {
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS place");
        sqlitedatabase.execSQL("DROP TABLE IF EXISTS vote");
        onCreate(sqlitedatabase);
    }

    public List searchPlacesWithKeyword(String s)
    {
        SQLiteDatabase sqlitedatabase = getReadableDatabase();
        String as[] = new String[1];
        as[0] = (new StringBuilder()).append("%").append(s).append("%").toString();
        Cursor cursor = sqlitedatabase.query("place", null, "name LIKE ?", as, null, null, null);
        ArrayList arraylist = new ArrayList();
        if (cursor != null)
        {
            for (; cursor.moveToNext(); arraylist.add(new Place(cursor.getString(cursor.getColumnIndex("id")), cursor.getString(cursor.getColumnIndex("name")), cursor.getString(cursor.getColumnIndex("address")), cursor.getString(cursor.getColumnIndex("password")), cursor.getString(cursor.getColumnIndex("city")), cursor.getString(cursor.getColumnIndex("country")), cursor.getDouble(cursor.getColumnIndex("latitude")), cursor.getDouble(cursor.getColumnIndex("longitude")), cursor.getString(cursor.getColumnIndex("passList")), cursor.getInt(cursor.getColumnIndex("failCount"))))) { }
            cursor.close();
        }
        return arraylist;
    }

    public int updatePlace(Place place)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("id", place.getParseID());
        contentvalues.put("name", place.getName());
        contentvalues.put("address", place.getAddress());
        contentvalues.put("password", place.getPassword());
        contentvalues.put("city", place.getCity());
        contentvalues.put("country", place.getCountry());
        contentvalues.put("latitude", Double.valueOf(place.getLatitude()));
        contentvalues.put("longitude", Double.valueOf(place.getLongitude()));
        Gson gson = new Gson();
        String json = gson.toJson(place.getPassList());
        contentvalues.put("passList", json);//place.getPassListStr());
        contentvalues.put("failCount", place.getFailCount());
        String as[] = new String[1];
        as[0] = place.getParseID();
        int i = sqlitedatabase.update("place", contentvalues, "id=?", as);
        sqlitedatabase.close();
        return i;
    }

    public int updateVote(Vote vote)
    {
        SQLiteDatabase sqlitedatabase = getWritableDatabase();
        ContentValues contentvalues = new ContentValues();
        contentvalues.put("placeID", vote.getPlaceID());
        contentvalues.put("id", vote.getId());
        contentvalues.put("password", vote.getPassword());
        String as[] = new String[1];
        as[0] = vote.getPlaceID();
        int i = sqlitedatabase.update("vote", contentvalues, "placeID=?", as);
        sqlitedatabase.close();
        return i;
    }
}