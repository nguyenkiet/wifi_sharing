package com.kietnt.freewifi.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import java.util.BitSet;
import java.util.List;

public class ConnectionManager
{
    public static interface GetWiFiResult
    {

        public abstract void done(List list);
    }

    public static int calculateWifiSingalStrength(int level)
    {
    	if(level > 80) return 1;
    	if(level > 60) return 2;
    	if(level > 40) return 3;
    	if(level > 20) return 4;
    	return 5;
    }
    private static ConnectionManager mInstance;
    private Context context;
    private WifiManager wifiManager;
    private BroadcastReceiver wifiReceiver;

    private ConnectionManager(Context context1)
    {
        wifiManager = (WifiManager)context1.getSystemService("wifi");
        context = context1;
    }

    public static ConnectionManager getInstance(Context context1)
    {
        if (mInstance == null)
        {
            mInstance = new ConnectionManager(context1);
        }
        return mInstance;
    }

    public boolean connectWiFi(ScanResult scanresult, String s)
    {
    	boolean flag;
    	int i;
        WifiConfiguration wificonfiguration;
        Log.d("wifi", (new StringBuilder()).append("connecting to ").append(scanresult.SSID).append(" with password ").append(s).toString());
        wificonfiguration = new WifiConfiguration();
        wificonfiguration.SSID = (new StringBuilder()).append("\"").append(scanresult.SSID).append("\"").toString();
        wificonfiguration.BSSID = scanresult.BSSID;
        wificonfiguration.status = 2;
        if (!scanresult.capabilities.contains("WPA") && !scanresult.capabilities.contains("WPA2"))
    	{
        	boolean flag1;
            flag1 = scanresult.capabilities.contains("WEP");
            flag = false;
            if (!flag1) 
            {
            	return flag;
            }
            else 
            {
            	wificonfiguration.wepKeys[0] = (new StringBuilder()).append("\"").append(s).append("\"").toString();
                wificonfiguration.wepTxKeyIndex = 0;
                wificonfiguration.allowedKeyManagement.set(0);
                wificonfiguration.allowedGroupCiphers.set(0);
                i = wifiManager.addNetwork(wificonfiguration);
                Log.d("wifi", (new StringBuilder()).append("add Network returned ").append(i).toString());
                flag = wifiManager.enableNetwork(i, true);
            }
    	}
        else
        {
        	wificonfiguration.preSharedKey = (new StringBuilder()).append("\"").append(s).append("\"").toString();
            
            if (scanresult.capabilities.contains("WPA2"))
            {
                wificonfiguration.allowedProtocols.set(1);
            } else
            {
                wificonfiguration.allowedProtocols.set(0);
            }
            wificonfiguration.allowedKeyManagement.set(1);
            wificonfiguration.allowedKeyManagement.set(2);
            wificonfiguration.allowedGroupCiphers.set(2);
            wificonfiguration.allowedGroupCiphers.set(3);
            wificonfiguration.allowedGroupCiphers.set(0);
            wificonfiguration.allowedGroupCiphers.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(1);
            wificonfiguration.allowedPairwiseCiphers.set(2);
        } 
        return true;
    }

    public String getConnectedWiFi()
    {
        if (((ConnectivityManager)context.getSystemService("connectivity")).getNetworkInfo(1).isConnected())
        {
            WifiInfo wifiinfo = wifiManager.getConnectionInfo();
            Log.d("wifi", (new StringBuilder()).append("device is connected to ").append(wifiinfo.getSSID()).toString());
            return wifiinfo.getSSID().replace("\"", "");
        } else
        {
            return null;
        }
    }

    public boolean locationServiceEnabled()
    {
        return ((LocationManager)context.getSystemService("location")).isProviderEnabled("network");
    }

    public void scanWiFi(final GetWiFiResult callback)
    {
        if (!wifiManager.isWifiEnabled())
        {
            wifiManager.setWifiEnabled(true);
        }
        wifiReceiver = new BroadcastReceiver() 
        {
            public void onReceive(Context context1, Intent intent)
            {
            	try{
                context1.unregisterReceiver(wifiReceiver);
                List results = wifiManager.getScanResults();
                if(callback != null) callback.done(results);
            	}
            	catch(Exception ex)
            	{
            		return;
            	}
            }
        };
        context.registerReceiver(wifiReceiver, new IntentFilter("android.net.wifi.SCAN_RESULTS"));
        wifiManager.startScan();
    }
}
