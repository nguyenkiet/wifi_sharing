package com.kietnt.freewifi;

import android.app.Application;
import com.kietnt.freewifi.helper.SessionManager;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import java.util.HashMap;

public class MainApplication extends Application
{ 
    public void onCreate()
    {
        super.onCreate();
        ParseUser.enableAutomaticUser();
        ParseUser.getCurrentUser().increment("RunCount");
        ParseUser.getCurrentUser().saveInBackground();
        ParseInstallation.getCurrentInstallation().saveInBackground();
        SessionManager.createInstance(getApplicationContext());
        @SuppressWarnings("unused")
		float density = getResources().getDisplayMetrics().density;
    }
}
