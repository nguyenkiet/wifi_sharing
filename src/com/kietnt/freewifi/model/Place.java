package com.kietnt.freewifi.model;

import android.content.Context;
import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;
import com.kietnt.freewifi.helper.DatabaseHelper;
import com.kietnt.freewifi.helper.SessionManager;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.parse.DeleteCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Place
    implements Parcelable
{
    public static interface OnCreateNewPlaceCallback
    {

        public abstract void done(Place place, Vote vote);
    }

    public static interface OnUpdateCallback
    {

        public abstract void done(boolean flag);
    }

    
    public static interface OnSendNewPasswordCallback
    {

        public abstract void done(boolean flag, Vote vote);
    }


    public static final android.os.Parcelable.Creator CREATOR = new android.os.Parcelable.Creator() 
    {

        public Place createFromParcel1(Parcel parcel)
        {
            return new Place(parcel);
        }

        public Object createFromParcel(Parcel parcel)
        {
            return createFromParcel1(parcel);
        }

        public Place[] newArray1(int i)
        {
            return new Place[i];
        }

        public Object[] newArray(int i)
        {
            return newArray1(i);
        }

    };
    private String address;
    private String city;
    private String country;
    private double latitude;
    private double longitude;
    private String name;
    private String parseID;
    private List<PasswordItem> passList;
    private String password;
    private int failCount;
 

    @SuppressWarnings("unchecked")
	public Place(Parcel parcel)
    {
        parseID = parcel.readString();
        name = parcel.readString();
        address = parcel.readString();
        password = parcel.readString();
        city = parcel.readString();
        country = parcel.readString();
        latitude = parcel.readDouble();
        longitude = parcel.readDouble();
        String passListStr = parcel.readString();
        failCount = parcel.readInt();
        setPassList(passListStr);
    }

    public Place(ParseObject parseobject)
    {
        parseID = parseobject.getObjectId();
        name = parseobject.getString("name");
        address = parseobject.getString("address");
        password = PasswordSecurity.encodedPassword(parseobject.getString("currentPass"));
        city = parseobject.getString("city_ggl");
        country = parseobject.getString("country");
        ParseGeoPoint parsegeopoint = parseobject.getParseGeoPoint("location");
        latitude = parsegeopoint.getLatitude();
        longitude = parsegeopoint.getLongitude();
        passList = new ArrayList<PasswordItem>();
        JSONArray jsonarray = parseobject.getJSONArray("passList");
        failCount = parseobject.getInt("failCount");
        if(jsonarray == null) return;
        int i = 0;
        while(true)
        {
	        if (i >= jsonarray.length())
	        {
	            break;
	        }
	        JSONObject jsonobject = null;
			try {
				jsonobject = jsonarray.getJSONObject(i);
			} catch (JSONException e) { 
			}
	        PasswordItem passworditem = new PasswordItem(jsonobject);
	        try {
				jsonobject.put("password", passworditem.getPassword());
			} catch (JSONException e) {
			}
	        passList.add(passworditem);
	        i++;
        }
		return;
    }

    public Place(String s, String s1, String s2)
    {
        name = s;
        address = s1;
        password = s2;
        failCount = 0;
    }

    public Place(String s, String s1, String s2, String s3, String s4, String s5, double d, double d1, String s6, int fCount)
    {
        parseID = s;
        name = s1;
        address = s2;
        password = s3;
        city = s4;
        country = s5;
        latitude = d;
        longitude = d1;
        failCount = fCount;
        setPassList(s6);
    }
    
    public Place(String s, String s1, String s2, List list)
    {
        name = s;
        address = s1;
        password = s2;
        passList = list;
        failCount = 0;
    }
    
    public Place()
    {
    	
    }
    public void updatePlace(Context context, final OnUpdateCallback callback)
    {
    	ParseObject parse = this.toParseObject();
    	parse.setObjectId(this.getParseID()); 
    	parse.saveInBackground(new SaveCallback() 
        {
        	public void done(ParseException ex)
        	{ 
        		if(ex == null)
        		{
                    if (callback != null)
                    {
                    	callback.done(true);
                    }
        		}
        		else
        		{
                    if (callback != null)
                    {
                    	callback.done(false);
                        return;
                    }
        		}
        	}
        });
    }
    public void deletePlace(Context context, final OnUpdateCallback callback)
    {
    	ParseObject parse = this.toParseObject();
    	parse.setObjectId(this.getParseID()); 
    	parse.deleteInBackground(new DeleteCallback()
    	{
			@Override
			public void done(ParseException arg0) {
				if(callback != null) callback.done(true);
			}
		});
    }
    
    public void createNewWithLocation(Context context, final Location location, final String s, final String s1, final OnCreateNewPlaceCallback oncreatenewplacecallback)
    {
        SessionManager.getInstance();
        SessionManager.getLocationInfo(context, location, new com.kietnt.freewifi.helper.SessionManager.GetLocationAddressCallback()
        {
        	//s, s1, location, oncreatenewplacecallback
            public void onComplete(boolean flag, String s2, String s3, String s4)
            {
                if (s2 != null)
                {
                	SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                	PasswordItem itemPwd = new PasswordItem(s1, 1, simpledateformat.format(new Date()), 0);
                	ArrayList<PasswordItem> lstVote = new ArrayList<PasswordItem>();
                	lstVote.add(itemPwd);
                    final Place place = new Place();
                    place.setName(s);
                    place.setAddress(s2);
                    place.setCity(s3);
                    place.setCountry(s4);
                    place.setPassword(s1);
                    place.setFailCount(0);
                    place.setLatitude(location.getLatitude());
                    place.setLongitude(location.getLongitude());
                    place.setPassList(lstVote);
                    final ParseObject parseobjectPlace = place.toParseObject();
                    parseobjectPlace.saveInBackground(new SaveCallback() 
                    {
                        public void done(ParseException parseexception)
                        {
                            if (parseexception == null)
                            {
                                place.setParseID(parseobjectPlace.getObjectId());
                                ParseUser.getCurrentUser().increment("shared");
                                ParseUser.getCurrentUser().saveInBackground();
                                final ParseObject parseobject = Vote.parseNewObject(parseobjectPlace.getObjectId(), password);
                                parseobject.saveInBackground(new SaveCallback() 
                                {
                                    public void done(ParseException parseexception)
                                    {
                                        if (parseexception == null)
                                        {
                                            Vote vote = new Vote(parseobject);
                                            place.setPassword(PasswordSecurity.encodedPassword(place.password));
                                            SessionManager.getInstance().setLastShareTime(new Date());
                                            if (oncreatenewplacecallback != null)
                                            {
                                            	oncreatenewplacecallback.done(place, vote);
                                            }
                                        } else
                                        {
                                            Log.d("parse", (new StringBuilder()).append("error vote ").append(parseexception.getMessage()).toString());
                                            if (oncreatenewplacecallback != null)
                                            {
                                            	oncreatenewplacecallback.done(null, null);
                                                return;
                                            }
                                        }
                                    }
                                });
                            } else
                            {
                                Log.d("parse", (new StringBuilder()).append("error place ").append(parseexception.getMessage()).toString());
                                if (oncreatenewplacecallback != null)
                                {
                                	oncreatenewplacecallback.done(null, null);
                                    return;
                                }
                            }
                        }
                    });
                } else
                if (oncreatenewplacecallback != null)
                {
                	oncreatenewplacecallback.done(null, null);
                    return;
                }
            }
        });
    }

    @SuppressWarnings("unchecked")
	public void sendNewPassword(Context context, String s, final String s1, final Place place, final boolean voting, final boolean deVote, final OnSendNewPasswordCallback onsendnewpasswordcallback)
    {
        final Vote vote = DatabaseHelper.getInstance(context).getVoteByPlaceID(s);
        if (vote != null)
        {
            if (vote.getPassword().equals(PasswordSecurity.encodedPassword(s1)) && !deVote && !voting)
            {
                if (onsendnewpasswordcallback != null)
                {
                    onsendnewpasswordcallback.done(true, null);
                }
                return; 
            }
            else
            {
                ParseObject.createWithoutData("Vote", vote.getId()).fetchInBackground(new GetCallback() 
                {
                	//s1, vote, onsendnewpasswordcallback
                    public void done(final ParseObject parseobject1, ParseException parseexception)
                    {
                        JSONArray jsonarray = parseobject1.getJSONArray("voteList");
                        boolean flag;
                        int i;
                        flag = false;
                        i = 0;
                        while(true)
                        {
	                        if (i >= jsonarray.length())
	                        {
	                            break;
	                        }
	                        JSONObject jsonobject1 = null;
							try {
								jsonobject1 = jsonarray.getJSONObject(i);
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
	                        try {
								jsonobject1.put("current", false);
							} catch (JSONException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
	                        try {
								if (jsonobject1.getString("password").equals(s1) && !voting)
								{
									onsendnewpasswordcallback.done(false, null);
									// Neu password da gui len roi thi khong gui nua
								    return;
								}
							} catch (JSONException e1) {
	
							}
	                        i++;
                        }
                        //Object moi
                        JSONObject jsonobject = new JSONObject();
                        try {
							jsonobject.put("password", s1);
						} catch (JSONException e) {

						}
                        try {
							jsonobject.put("current", true);
						} catch (JSONException e) {

						}
                        try {
							jsonobject.put("lastSync", false);
						} catch (JSONException e) {

						}
                        jsonarray.put(jsonobject);
                        // Save objct
                        parseobject1.put("synced", Boolean.valueOf(false));
                        parseobject1.saveInBackground(new SaveCallback() 
                        {
                            public void done(ParseException parseexception)
                            {
                                if (parseexception == null)
                                {
                                	vote.setPassword(PasswordSecurity.encodedPassword(s1));
                                    SessionManager.getInstance().setLastVoteTime(new Date());
                                    // Cap nhat lai value trong PlaceObject
                                    SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                    String date = simpledateformat.format(parseobject1.getUpdatedAt());
                                    PasswordItem pwd = new PasswordItem(s1, 1, date, 0);
                                    if(!deVote) 
                                    {
                                    	place.addPassList(pwd);
                                    }
                                    else 
                                	{
                                		vote.setRemoved(place.deVotePass(pwd)); 
                                	}
                                    ParseObject.createWithoutData("Place", vote.getPlaceID()).fetchInBackground(new GetCallback() 
                                    {
                                    	public void done(final ParseObject placeRes, ParseException ex)
                                        {
                                    		if(ex == null)
                                    		{
                                    			Gson gson = new Gson();
                                    	        String json = gson.toJson(place.getPassList());
                                    	        try {
                                    	        	placeRes.put("currentPass", place.getPassword());
                                    	        	placeRes.put("passList", new JSONArray(json));
                                    			} 
                                    	        catch (JSONException e) 
                                    	        {
                                    				if (onsendnewpasswordcallback != null)
                                                    {
                                                    	onsendnewpasswordcallback.done(false, null);
                                                        return;
                                                    }
                                    			}
                                    			placeRes.saveInBackground(new SaveCallback() 
                                                {
                                                	public void done(ParseException ex)
                                                	{ 
                                                		if(ex == null)
                                                		{
            	                                            if (onsendnewpasswordcallback != null)
            	                                            {
            	                                            	onsendnewpasswordcallback.done(true, vote);
            	                                            }
                                                		}
                                                		else
                                                		{
                                                            if (onsendnewpasswordcallback != null)
                                                            {
                                                            	onsendnewpasswordcallback.done(false, null);
                                                                return;
                                                            }
                                                		}
                                                	}
                                                });
                                    		}
                                    		else
                                    		{
                                    			if (onsendnewpasswordcallback != null)
                                                {
                                                	onsendnewpasswordcallback.done(false, null);
                                                    return;
                                                }
                                    		}
                                        }
                                    }); 
                                } 
                                else
                                if (onsendnewpasswordcallback != null)
                                {
                                	onsendnewpasswordcallback.done(false, null);
                                    return;
                                }
                            } 
                        });
                    }
                });
                return;
            }
        } 
        else
        {
            final ParseObject parseobject = Vote.parseNewObject(s, s1);
            parseobject.saveInBackground(new SaveCallback() {
                public void done(ParseException parseexception)
                {
                    if (parseexception == null)
                    {
                        SessionManager.getInstance().setLastVoteTime(new Date());
                        if (onsendnewpasswordcallback != null)
                        {
                        	onsendnewpasswordcallback.done(true, new Vote(parseobject));
                        }
                    } else
                    if (onsendnewpasswordcallback != null)
                    {
                    	onsendnewpasswordcallback.done(false, null);
                        return;
                    }
                } 
            });
            return;
        }
    }

    public int describeContents()
    {
        return 0;
    }

    public String getAddress()
    {
        return address;
    }
    
    public int getFailCount()
    {
    	return failCount;
    }
    
    public void setFailCount(int fCount)
    {
    	failCount = fCount;
    }
    public String getCity()
    {
        return city;
    }

    public String getCountry()
    {
        return country;
    }

    public LatLng getLatLng()
    {
        return new LatLng(latitude, longitude);
    }

    public double getLatitude()
    {
        return latitude;
    }

    public Location getLocation()
    {
        Location location = new Location(name);
        location.setLatitude(latitude);
        location.setLongitude(longitude);
        return location;
    }

    public double getLongitude()
    {
        return longitude;
    }

    public String getName()
    {
        return name;
    }

    public String getParseID()
    {
        return parseID;
    }

    public List getPassList()
    {
        return passList;
    }
     
    public String getPassword()
    {
        return password;
    }

    public void setAddress(String s)
    {
        address = s;
    }

    public void setCity(String s)
    {
        city = s;
    }

    public void setCountry(String s)
    {
        country = s;
    }

    public void setLatitude(double d)
    {
        latitude = d;
    }

    public void setLongitude(double d)
    {
        longitude = d;
    }

    public void setName(String s)
    {
        name = s;
    }

    public void setParseID(String s)
    {
        parseID = s;
    }

    public void setPassList(List list)
    {
        passList = list;
    }
    public boolean deVotePass(PasswordItem pass)
    {
    	PasswordItem itm = null;
    	boolean removed = false;
    	for(int i =0 ; i<passList.size(); i++)
    	{    		
    		itm = passList.get(i);
    		if(itm.getPassword() == pass.getPassword())
    		{
    			itm.setDeVoteCount(itm.getDeVoteCount() + 1);
    			if(itm.getDeVoteCount() >= SessionManager.MAX_WRONG_NOTIFY)
    			{
	    			removed = true;
	    			break;
    			}
    		}
    	}
    	if(removed) passList.remove(itm);
    	return removed;
    }
    public void addPassList(PasswordItem pass)
    {
    	PasswordItem itm = null;
    	int vote = 0;
    	boolean added = true;
    	for(int i =0 ; i<passList.size(); i++)
    	{    		
    		itm = passList.get(i);
    		if(itm.getVoteCount() > vote) 
			{
    			vote = itm.getVoteCount();
				setPassword(itm.getPassword());
			}
    		if(itm.getPassword() == pass.getPassword())
    		{
    			itm.setVoteCount(itm.getVoteCount() + 1);
    			itm.setDateString(pass.getDateString());
    			added = false;
    		}
    	}
    	if(added) passList.add(pass);
    }
    public void setPassList(String jsonString)
    {
    	Log.e("kietnt.8x--setpassList", jsonString);
        passList = new ArrayList<PasswordItem>();
        JSONArray jsonarray = null;
		try {
			jsonarray = new JSONArray(jsonString);
		} catch (JSONException e1) {
			Gson gson = new Gson();
			Log.e("kietnt.8x--error password", gson.toJson(e1));
			e1.printStackTrace();
		}
        int i = 0;
		while(true)
		{
	        if (i >= jsonarray.length())
	        {
	            break; 
	        }
	        JSONObject jsonobject = null;
			try {
				jsonobject = jsonarray.getJSONObject(i);
			} catch (JSONException e) {
				Gson gson = new Gson();
				Log.e("kietnt.8x--error password", gson.toJson(e));
				e.printStackTrace();
			}
	        PasswordItem passworditem = new PasswordItem(jsonobject);		        
	        passList.add(passworditem);
	        i++;
		}
    } 
    
    public void setPassword(String s)
    {
        password = s;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	public void sortPassList()
    {
        Collections.sort(passList, new Comparator() {
 
            public int compare(PasswordItem passworditem, PasswordItem passworditem1)
            {
                long l = passworditem.getCreatedAt().getTime();
                long l1 = passworditem1.getCreatedAt().getTime();
                if (l > l1)
                {
                    return -1;
                }
                return l >= l1 ? 0 : 1;
            }

            public int compare(Object obj, Object obj1)
            {
                return compare((PasswordItem)obj, (PasswordItem)obj1);
            } 
        });
    }

    public ParseObject toParseObject()
    {
        ParseObject parseobject = ParseObject.create("Place");
        parseobject.put("name", name);
        parseobject.put("currentPass", password);
        parseobject.put("address", address);
        parseobject.put("city_ggl", city);
        parseobject.put("country", country);
        ParseGeoPoint parsegeopoint = new ParseGeoPoint();
        parsegeopoint.setLatitude(latitude);
        parsegeopoint.setLongitude(longitude);
        parseobject.put("location", parsegeopoint);
        Gson gson = new Gson();
        String json = gson.toJson(getPassList());
        try {
			parseobject.put("passList", new JSONArray(json));
		} catch (JSONException e) {
			parseobject.put("passList", new ArrayList());
		}
        parseobject.put("failCount", failCount);
        //parseobject.put("tempPassList", new ArrayList());
        return parseobject;
    }

    public void writeToParcel(Parcel parcel, int i)
    {
        parcel.writeString(parseID);
        parcel.writeString(name);
        parcel.writeString(address);
        parcel.writeString(password);
        parcel.writeString(city);
        parcel.writeString(country);
        parcel.writeDouble(latitude);
        parcel.writeDouble(longitude);
        //Change to string
        Gson gson = new Gson();
        String json = gson.toJson(getPassList());
        parcel.writeString(json);
        parcel.writeInt(failCount);
    }
}