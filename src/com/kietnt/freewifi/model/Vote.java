package com.kietnt.freewifi.model;

import android.util.Log;
import com.parse.ParseObject;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Vote
{

    private String id;
    private String password;
    private String placeID;
    private boolean removed;

    public Vote()
    {
    	removed = false;
    }

    public Vote(ParseObject parseobject)
    {
    	removed = false;
        id = parseobject.getObjectId();
        placeID = parseobject.getString("place");
        password = "";
        JSONArray jsonarray;
        int i;
        JSONObject jsonobject = null;
        try
        {
            jsonarray = parseobject.getJSONArray("voteList");
        }
        catch (Exception jsonexception)
        {
            Log.d("parse", (new StringBuilder()).append("error ").append(jsonexception.getMessage()).toString());
            return;
        }
        if (0 < jsonarray.length())
        {
            try {
				jsonobject = jsonarray.getJSONObject(0);
			} catch (JSONException e) {
				Log.d("parse", (new StringBuilder()).append("error ").append(e.getMessage()).toString());		            
			}
            try {
				if (!jsonobject.getBoolean("current"))
				{
				    return;
				}
			} catch (JSONException e) {
				Log.d("parse", (new StringBuilder()).append("error ").append(e.getMessage()).toString());
			}
            try {
				password = PasswordSecurity.encodedPassword(jsonobject.getString("password"));
			} catch (JSONException e) {
				Log.d("parse", (new StringBuilder()).append("error ").append(e.getMessage()).toString());
			}
        }
        return;
    }

    public Vote(String s, String s1, String s2)
    {
    	removed = false;
        id = s1;
        placeID = s;
        password = s2;
    }

    public static ParseObject parseNewObject(String s, String s1)
    {
        ParseObject parseobject = ParseObject.create("Vote");
        parseobject.put("place", s);
        parseobject.put("synced", Boolean.valueOf(false));
        JSONObject jsonobject = new JSONObject();
        JSONArray jsonarray;
        try
        {
            jsonobject.put("password", s1);
            jsonobject.put("current", true);
            jsonobject.put("lastSync", false);
        }
        catch (JSONException jsonexception)
        {
            Log.d("vote", (new StringBuilder()).append("error ").append(jsonexception.getMessage()).toString());
        }
        jsonarray = new JSONArray();
        jsonarray.put(jsonobject);
        parseobject.put("voteList", jsonarray);
        return parseobject;
    }

    public String getId()
    {
        return id;
    }

    public String getPassword()
    {
    	if(password == null) setPassword("N/A");
        return password;
    }

    public String getPlaceID()
    {
        return placeID;
    }

    public void setId(String s)
    {
        id = s;
    }

    public void setPassword(String s)
    {
        password = s;
    }

    public void setRemoved(boolean s)
    {
        removed = s;
    }
    
    public boolean getRemoved()
    {
        return removed;
    }

    public void setPlaceID(String s)
    {
        placeID = s;
    }
}