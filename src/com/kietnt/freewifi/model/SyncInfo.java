package com.kietnt.freewifi.model;

import java.util.Date;

public class SyncInfo
{

    private String city;
    private String country;
    private Date lastSync;
    private int totalPlaces;

    public SyncInfo()
    {
    }

    public SyncInfo(String s, String s1, Date date)
    {
        city = s;
        country = s1;
        lastSync = date;
        totalPlaces = -1;
    }

    public String getCity()
    {
        return city;
    }

    public String getCountry()
    {
        return country;
    }

    public Date getLastSync()
    {
        return lastSync;
    }

    public int getTotalPlaces()
    {
        return totalPlaces;
    }

    public void setCity(String s)
    {
        city = s;
    }

    public void setCountry(String s)
    {
        country = s;
    }

    public void setLastSync(Date date)
    {
        lastSync = date;
    }

    public void setTotalPlaces(int i)
    {
        totalPlaces = i;
    }
}