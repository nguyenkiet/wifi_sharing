package com.kietnt.freewifi.model;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.Gson;

public class PasswordItem
{

    private String dateString;
    private String password;
    private int voteCount;
    private int deVoteCount;

    public PasswordItem()
    {
    }

    public PasswordItem(String pwd, int i, String stringDate, int deVote)
    {
        password = pwd;
        voteCount = i;
        dateString = stringDate;
        deVoteCount = deVote;
    }

    public PasswordItem(JSONObject jsonobject)
    {
        try
        {
        	//Gson gson = new Gson();
        	//Log.e("kietnt-info-dateString", gson.toJson(jsonobject));
        	if(jsonobject.has("password"))
        		password = PasswordSecurity.encodedPassword(jsonobject.getString("password"));
            if(jsonobject.has("voteCount"))
            	voteCount = jsonobject.getInt("voteCount");
            if(jsonobject.has("deVoteCount"))
            	deVoteCount = jsonobject.getInt("deVoteCount");
            if(jsonobject.has("dateString"))
            	dateString = jsonobject.getString("dateString");
            return;
        }
        catch (JSONException jsonexception)
        {
        	//Gson gson = new Gson();
        	//Log.e("kietnt-error-dateString", jsonexception.getMessage());
            return;
        }
    }

    public Date getCreatedAt()
    {
    	if(dateString == null) return new Date();
        SimpleDateFormat simpledateformat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");        
        Date date;
        try
        {
            date = simpledateformat.parse(dateString);
        }
        catch (ParseException parseexception)
        {
            return null;
        }
        return date;
    }

    public String getDateDescription()
    {
        Date date = new Date();
        Date date1 = getCreatedAt();
        long l = Math.abs((date.getTime() - date1.getTime()) / 1000L);
        if (l < 60L)
        {
            return (new StringBuilder()).append(l).append(" gi\342y tr\u01B0\u1EDBc").toString();
        }
        if (l < 3600L)
        {
            return (new StringBuilder()).append((int)(l / 60L)).append(" ph\372t tr\u01B0\u1EDBc").toString();
        }
        if (l < 0x15180L)
        {
            return (new StringBuilder()).append((int)(l / 3600L)).append(" gi\u1EDD tr\u01B0\u1EDBc").toString();
        }
        if (l < 0x2a300L)
        {
            return "ng\340y h\364m qua";
        }
        if (l < 0x93a80L)
        {
            return (new StringBuilder()).append((int)(l / 0x15180L)).append(" ng\340y tr\u01B0\u1EDBc").toString();
        } else
        {
            return (new SimpleDateFormat("dd/MM/yyyy")).format(date1);
        }
    }

    public String getDateString()
    {
        return dateString;
    }

    public String getPassword()
    {
    	if(password == null) setPassword("N/A");
        return password;
    }

    public int getVoteCount()
    {
        return voteCount;
    }
    public int getDeVoteCount()
    {
    	return deVoteCount;
    }
    public String getVoteCountStr()
    {
        return (new StringBuilder()).append(voteCount).append(" ng\u01B0\u1EDDi \u0111\343 b\354nh ch\u1ECDn").toString();
    }

    public void setDateString(String s)
    {
        dateString = s;
    }

    public void setPassword(String s)
    {
        password = s;
    }

    public void setDeVoteCount(int i)
    {
    	deVoteCount = i;
    }
    public void setVoteCount(int i)
    {
        voteCount = i;
    }
}