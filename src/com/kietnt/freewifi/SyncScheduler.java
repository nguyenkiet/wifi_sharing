package com.kietnt.freewifi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.kietnt.freewifi.helper.SessionManager;

// Referenced classes of package com.kietnt.freewifi:
//            SyncService

public class SyncScheduler extends BroadcastReceiver
{

    public static final int REQUEST_CODE = 12345;

    public void onReceive(Context context, Intent intent)
    {
        SessionManager.createInstance(context);
        if (!SessionManager.getInstance().haveRunningService() && SessionManager.getInstance().getAutoSync())
        {
            context.startService(new Intent(context, SyncService.class));
        }
    }
}
