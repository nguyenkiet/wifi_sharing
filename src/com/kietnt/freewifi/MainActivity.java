package com.kietnt.freewifi;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import com.kietnt.freewifi.R;
import com.kietnt.freewifi.helper.SessionManager;
import com.kietnt.freewifi.layout.HomeFragment;
import com.kietnt.freewifi.layout.InfoFragment;
import com.kietnt.freewifi.layout.MapActivity;
import com.kietnt.freewifi.layout.SettingFragment;
import com.kietnt.freewifi.layout.SyncFragment;
import com.kietnt.freewifi.layout.adapter.NavDrawerListAdapter;
import com.kietnt.freewifi.layout.model.NavDrawerItem;
import com.google.android.gms.analytics.GoogleAnalytics;
import java.util.ArrayList;

// Referenced classes of package com.kietnt.freewifi:
//            SyncScheduler, WelcomeActivity

public class MainActivity extends Activity
{
    private class SlideMenuClickListener
        implements android.widget.AdapterView.OnItemClickListener
    {
        public void onItemClick(AdapterView adapterview, View view, int i, long l)
        {
            displayView(i);
        }
    }


    private NavDrawerListAdapter adapter;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private CharSequence mDrawerTitle;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private ArrayList navDrawerItems;
    private TypedArray navMenuIcons;
    private String navMenuTitles[];
 
    private void cancelAlarm()
    {
        Log.d("alarm", "cancel alarm");
        PendingIntent pendingintent = PendingIntent.getBroadcast(this, 12345, new Intent(getApplicationContext(), com.kietnt.freewifi.SyncScheduler.class), Integer.MAX_VALUE);
        ((AlarmManager)getSystemService("alarm")).cancel(pendingintent);
    }

    private void displayView(int i)
    {
        Object obj = null;
        if(i == 0) obj = new com.kietnt.freewifi.layout.HomeFragment();  
        if(i == 1) obj = new com.kietnt.freewifi.layout.SettingFragment(); 
        if(i == 2) obj = new com.kietnt.freewifi.layout.SyncFragment(); 
        if(i == 3) obj = new com.kietnt.freewifi.layout.InfoFragment();         
        if (obj != null)
        {
            getFragmentManager().beginTransaction().replace(R.id.frame_container, ((android.app.Fragment) (obj))).commit();
            mDrawerList.setItemChecked(i, true);
            mDrawerList.setSelection(i);
            setTitle(navMenuTitles[i]);
            mDrawerLayout.closeDrawer(mDrawerList);
            return;
        } 
        else
        {
            Log.e("MainActivity", "Error in creating fragment");
            return;
        } 
    }

    private void startAlarm()
    {
        Log.d("alarm", "start alarm");
        PendingIntent pendingintent = PendingIntent.getBroadcast(this, 12345, new Intent(getApplicationContext(), SyncScheduler.class), 0x8000000);
        long l = System.currentTimeMillis();
        int i = 1000 * SessionManager.SYNC_CHECK_TIME;
        ((AlarmManager)getSystemService("alarm")).setRepeating(0, l, i, pendingintent);
    }

    public void onConfigurationChanged(Configuration configuration)
    {
        super.onConfigurationChanged(configuration);
        mDrawerToggle.onConfigurationChanged(configuration);
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	protected void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setContentView(R.layout.activity_main);
        CharSequence charsequence = getTitle();
        mDrawerTitle = charsequence;
        mTitle = charsequence;
        int version = 0;
        try {
			version = getPackageManager().getPackageInfo("com.google.android.gms", 0 ).versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);
        navMenuIcons = getResources().obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerList = (ListView)findViewById(R.id.list_slidermenu);
        navDrawerItems = new ArrayList();
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
        navMenuIcons.recycle();
        mDrawerList.setOnItemClickListener(new SlideMenuClickListener());
        adapter = new NavDrawerListAdapter(getApplicationContext(), navDrawerItems);
        mDrawerList.setAdapter(adapter);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.drawable.ic_drawer, R.string.app_name, R.string.app_name) 
        {
            public void onDrawerClosed(View view)
            {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View view)
            {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu();
            }

        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        if (bundle == null)
        {
            displayView(0);
        }
    }

    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.main, menu);
        SearchManager searchmanager = (SearchManager)getSystemService("search");
        ((SearchView)menu.findItem(R.id.action_searchPlace).getActionView()).setSearchableInfo(searchmanager.getSearchableInfo(getComponentName()));
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem menuitem)
    {
        if (!mDrawerToggle.onOptionsItemSelected(menuitem)) 
        {
        	switch (menuitem.getItemId())
            {
            default:
                return super.onOptionsItemSelected(menuitem);

            case R.id.action_searchPlace: 
                break;

            case R.id.action_showMap: 
                startActivity(new Intent(this, MapActivity.class));
                break;
            }
        }
        return true;
    }

    protected void onPostCreate(Bundle bundle)
    {
        super.onPostCreate(bundle);
        mDrawerToggle.syncState();
    }

    public boolean onPrepareOptionsMenu(Menu menu)
    {
        boolean flag = mDrawerLayout.isDrawerOpen(mDrawerList);
        MenuItem menuitem = menu.findItem(R.id.action_searchPlace);
        boolean flag1;
        if (!flag)
        {
            flag1 = true;
        } else
        {
            flag1 = false;
        }
        menuitem.setVisible(flag1);
        return super.onPrepareOptionsMenu(menu);
    }

    protected void onResume()
    {
        super.onResume();
    }

    protected void onStart()
    {
        super.onStart();
        if (SessionManager.getInstance().isFirstTimeUsingApp())
        {
            startActivity(new Intent(this, WelcomeActivity.class));
        } else
        {
            cancelAlarm();
            startAlarm();
        }
        GoogleAnalytics.getInstance(this).reportActivityStart(this);
    }

    protected void onStop()
    {
        GoogleAnalytics.getInstance(this).reportActivityStop(this);
        super.onStop();
    }

    public void setTitle(CharSequence charsequence)
    {
        mTitle = charsequence;
        getActionBar().setTitle(mTitle);
    } 
}
